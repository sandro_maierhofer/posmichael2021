﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De.SandroMaierhofer.POS_Michael.Logic.Ui.Messages
{
    public class OpenTablesWindow
    {
        public OpenTablesWindow(object mw)
        {
            this.Mw = mw;
        }
        public object Mw { get; set; }
    }
}
