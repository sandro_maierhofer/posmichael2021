﻿using De.SandroMaierhofer.POS_Michael.Business.Model.BusinessObjects;
using De.SandroMaierhofer.POS_Michael.Logic.Ui.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De.SandroMaierhofer.POS_Michael.Logic.Ui.Wrapper
{
    public class TableVM : ViewModelBase
    {

        private Table _Table;
        public Table Table
        {
            get { return _Table; }
            set
            {
                _Table = value;
                OnPropertyChanged(nameof(Table));
            }
        }

        private ObservableCollection<ProductVM> _ProductsVM;
        public ObservableCollection<ProductVM> ProductVM
        {
            get { return _ProductsVM; }
            set
            {
                _ProductsVM = value;
                OnPropertyChanged(nameof(ProductVM));
            }
        }

        private bool synchDisabled = false;

        #region Properties
        public string Name
        {
            get { return _Table.Name; }
            set
            {
                _Table.Name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        public int Status
        {
            get { return _Table.Status; }
            set
            {
                _Table.Status = value;
                OnPropertyChanged(nameof(Status));
            }
        }

        public double Sum
        {
            get { return _Table.Sum; }
            set
            {
                _Table.Sum = value;
                OnPropertyChanged(nameof(Sum));
            }
        }

        public int Id
        {
            get { return _Table.Id; }
            set
            {
                _Table.Id = value;
                OnPropertyChanged(nameof(Id));
            }
        }

        public int OrderId
        {
            get { return _Table.OrderId; }
            set
            {
                _Table.OrderId = value;
                OnPropertyChanged(nameof(OrderId));
            }
        }

        public int EmployeeID
        {
            get { return _Table.EmployeeID; }
            set
            {
                _Table.EmployeeID = value;
                OnPropertyChanged(nameof(EmployeeID));
            }
        }

        public int RechnungNr
        {
            get { return _Table.RechnungNr; }
            set
            {
                _Table.RechnungNr = value;
                OnPropertyChanged(nameof(RechnungNr));
            }
        }

        public string ShortName
        {
            get { return _Table.ShortName; }
            set
            {
                _Table.ShortName = value;
                OnPropertyChanged(nameof(ShortName));
            }
        }

        #endregion

        #region Sync

        private void VMChanged(object sender, NotifyCollectionChangedEventArgs e)
        {

            if (synchDisabled) return;

            synchDisabled = true;

            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (var m in e.NewItems.OfType<ProductVM>().Select(v => v.Product).OfType<Product>())
                        _Table.Products.Add(m);
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (var m in e.OldItems.OfType<ProductVM>().Select(v => v.Product).OfType<Product>())
                        _Table.Products.Remove(m);
                    break;
                case NotifyCollectionChangedAction.Reset:
                    _Table.Products.Clear();
                    break;
            }

            //Enable sync
            synchDisabled = false;
        }

        private void MChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            //check if sync is disabled or not
            if (synchDisabled) return;

            synchDisabled = true;

            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (var card in e.NewItems.OfType<Product>())
                        ProductVM.Add(new ProductVM(card));
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (var card in e.OldItems.OfType<Product>())
                        ProductVM.Remove(GetViewModelOfModel(card));
                    break;
                case NotifyCollectionChangedAction.Reset:
                    ProductVM.Clear();
                    break;
            }

            //Enable sync
            synchDisabled = false;
        }

        private ProductVM GetViewModelOfModel(Product card)
        {
            foreach (ProductVM cvm in ProductVM)
            {
                if (cvm.Product.Equals(card)) return cvm;
            }
            return null;
        }


        #endregion

        #region Konstruktor

        public TableVM() : base()
        {
            _Table = new Table();
            _ProductsVM = new ObservableCollection<ProductVM>();
            _Table.Products.CollectionChanged += MChanged;
            _ProductsVM.CollectionChanged += VMChanged;

        }

        public TableVM(Table cc) : base()
        {
            this._Table = cc;
            ProductVM = new ObservableCollection<ProductVM>();
            ProductVM.CollectionChanged += VMChanged;
        }
        //Konstruktor to copy a tablevm
        public TableVM(TableVM cc)
        {
            _Table = new Table
            {
                Products = new ObservableCollection<Product>(),
                ShortName = cc.ShortName,
                Status = cc.Status,
                Sum = cc.Sum,
                EmployeeID = cc.EmployeeID,
                Id = cc.Id,
                Name = cc.Name,
                OrderId = cc.OrderId,
                RechnungNr = cc.RechnungNr
            };
            _Table.Products.CollectionChanged += MChanged;

            ProductVM = new ObservableCollection<ProductVM>();
            ProductVM.CollectionChanged += VMChanged;

            foreach (ProductVM p in cc.ProductVM)
            {
                ProductVM.Add(new ProductVM(p));
            }

            OnPropertyChanged(nameof(Table));
        }

        #endregion
    }
}
