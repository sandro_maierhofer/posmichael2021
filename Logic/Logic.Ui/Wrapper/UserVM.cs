﻿using De.SandroMaierhofer.POS_Michael.Business.Model.BusinessObjects;
using De.SandroMaierhofer.POS_Michael.Logic.Ui.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De.SandroMaierhofer.POS_Michael.Logic.Ui.Wrapper
{
    public class UserVM : ViewModelBase
    {

        private User _User;

        public UserVM(UserVM user)
        {
            User = new User();
            ID = user.ID;
            Name = user.Name;
            Username = user.Username;
            Password = user.Password;
            PicPath = user.PicPath;
            Role = user.Role;
        }

        public UserVM()
        {
            User = new User();
        }

        public User User
        {
            get { return _User; }
            set
            {
                _User = value;
                OnPropertyChanged(nameof(User));
            }
        }

        public int ID
        {
            get { return _User.ID; }
            set
            {
                _User.ID = value;
                OnPropertyChanged(nameof(ID));
            }
        }
        public string Name
        {
            get { return _User.Name; }
            set
            {
                _User.Name = value;
                OnPropertyChanged(nameof(Name));
            }
        }
        public string Username
        {
            get { return _User.Username; }
            set
            {
                _User.Username = value;
                OnPropertyChanged(nameof(Username));
            }
        }
        public string Password
        {
            get { return _User.Password; }
            set
            {
                _User.Password = value;
                OnPropertyChanged(nameof(Password));
            }
        }
        public string PicPath
        {
            get { return _User.PicPath; }
            set
            {
                _User.PicPath = value;
                OnPropertyChanged(nameof(PicPath));
            }
        }
        public string Role
        {
            get { return _User.Role; }
            set
            {
                _User.Role = value;
                OnPropertyChanged(nameof(Role));
            }
        }
    }
}
