﻿using De.SandroMaierhofer.POS_Michael.Business.Model.BusinessObjects;
using De.SandroMaierhofer.POS_Michael.Logic.Ui.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De.SandroMaierhofer.POS_Michael.Logic.Ui.Wrapper
{
    public class ProductVM : ViewModelBase
    {
        private Product _Product;
        public Product Product
        {
            get { return _Product; }
            set
            {
                _Product = value;
                OnPropertyChanged(nameof(Product));
            }
        }

        public ProductVM()
        {
            _Product = new Product();

        }

        public ProductVM(Product product)
        {
            Product = new Product
            {
                product_name = product.product_name,
                product_code = product.product_code,
                description = product.description,
                category = product.category,
                count = product.count,
                tax = product.tax,
                price = product.price,
                ID = product.ID,
                img = product.img
            };
            OnPropertyChanged("Product");
        }

        public ProductVM(ProductVM p)
        {
            Product = new Product();
            Product.product_name = p.Product_name;
            Product.product_code = p.Product_code;
            Product.description = p.Description;
            Product.category = p.Category;
            Product.count = p.Count;
            Product.price = p.Price;
            Product.tax = p.Tax;
            Product.ID = p.ID;
            Product.img = p.Image;
            OnPropertyChanged(nameof(Product));
        }



        public string Product_code
        {
            get { return _Product.product_code; }
            set
            {
                _Product.product_code = value;
                OnPropertyChanged(nameof(Product_code));
            }
        }

        public string Product_name
        {
            get { return _Product.product_name; }
            set
            {
                _Product.product_name = value;
                OnPropertyChanged(nameof(Product_name));
            }
        }
        public string Description
        {
            get { return _Product.description; }
            set
            {
                _Product.description = value;
                OnPropertyChanged(nameof(Description));
            }
        }
        public int Count
        {
            get { return _Product.count; }
            set
            {
                _Product.count = value;
                OnPropertyChanged(nameof(Count));
            }
        }

        public int Category
        {
            get { return _Product.category; }
            set
            {
                _Product.category = value;
                OnPropertyChanged(nameof(Category));
            }
        }
        public int Tax
        {
            get { return _Product.tax; }
            set
            {
                _Product.tax = value;
                OnPropertyChanged(nameof(Tax));
            }
        }
        public double Price
        {
            get { return _Product.price; }
            set
            {
                _Product.price = value;
                OnPropertyChanged(nameof(Price));
            }
        }

        public string Image
        {
            get { return _Product.img; }
            set
            {
                _Product.img = value;
                OnPropertyChanged(nameof(Image));
            }
        }

        public int ID
        {
            get { return _Product.ID; }
            set
            {
                _Product.ID = value;
                OnPropertyChanged(nameof(ID));
            }
        }
    }
}
