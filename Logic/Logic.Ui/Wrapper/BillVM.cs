﻿using De.SandroMaierhofer.POS_Michael.Business.Model.BusinessObjects;
using De.SandroMaierhofer.POS_Michael.Logic.Ui.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De.SandroMaierhofer.POS_Michael.Logic.Ui.Wrapper
{
    public class BillVM : ViewModelBase
    {
        private Bill _Bill;
        public BillVM(BillVM bill)
        {
            _Bill = new Bill();
            _Bill.ID = bill.ID;
            _Bill.Order_Date = bill.Order_Date;
            _Bill.Order_ID = bill.Order_ID;
            _Bill.Euro = bill.Euro;
            _Bill.TransactionTyp = bill.TransactionTyp;
        }

        public BillVM()
        {
            _Bill = new Bill();

        }

        public int ID
        {
            get { return _Bill.ID; }
            set
            {
                _Bill.ID = value;
                OnPropertyChanged(nameof(ID));
            }
        }

        public int Order_ID
        {
            get { return _Bill.Order_ID; }
            set
            {
                _Bill.Order_ID = value;
                OnPropertyChanged(nameof(Order_ID));
            }
        }
        public double Euro
        {
            get { return _Bill.Euro; }
            set
            {
                _Bill.Euro = value;
                OnPropertyChanged(nameof(Euro));
            }
        }
        public DateTime Order_Date
        {
            get { return _Bill.Order_Date; }
            set
            {
                _Bill.Order_Date = value;
                OnPropertyChanged(nameof(Order_Date));
            }
        }

        public int TransactionTyp
        {
            get { return _Bill.TransactionTyp; }
            set
            {
                _Bill.TransactionTyp = value;
                OnPropertyChanged(nameof(TransactionTyp));
            }
        }
        public int Status
        {
            get { return _Bill.Status; }
            set
            {
                _Bill.Status = value;
                OnPropertyChanged(nameof(Status));
            }
        }
    }
}
