﻿using De.SandroMaierhofer.POS_Michael.Business.Model.BusinessObjects;
using De.SandroMaierhofer.POS_Michael.Logic.Ui.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De.SandroMaierhofer.POS_Michael.Logic.Ui.Wrapper
{
    public class ProductCategoryVM : ViewModelBase
    {
        private ProductCategory _PC;

        public ProductCategoryVM()
        {
            _PC = new ProductCategory();
        }

        public ProductCategory PC
        {
            get { return _PC; }
            set
            {
                _PC = value;
                OnPropertyChanged(nameof(_PC));
            }
        }

        public string Name
        {
            get { return _PC.Name; }
            set
            {
                _PC.Name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        public int ID
        {
            get { return _PC.ID; }
            set
            {
                _PC.ID = value;
                OnPropertyChanged(nameof(ID));
            }
        }

    }
}
