﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;


namespace De.SandroMaierhofer.POS_Michael.Logic.Ui.Services
{
    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        public Dictionary<string, string> PropertyMapping { get; set; }
        public ViewModelBase()
        {
            this.PropertyMapping = new Dictionary<string, string>();
        }
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void PropertyChangedHandler(object sender, PropertyChangedEventArgs e)
        {
            if (this.PropertyMapping.TryGetValue(e.PropertyName, out string propertyNameOut))
            {
                OnPropertyChanged(propertyNameOut);
            }
        }
    }
}
