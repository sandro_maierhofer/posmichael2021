﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace De.SandroMaierhofer.POS_Michael.Logic.Ui.Services
{
    public class BooleanVisibilityConverter : IValueConverter
    {
   

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // Call ButtonTextConverter here and return the result
            if ((bool)value == true)
            {
                return "Visible";
            }
            else
            {
                return "Hidden";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
