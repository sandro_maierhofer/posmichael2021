﻿using De.SandroMaierhofer.POS_Michael.Logic.Ui.Wrapper;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace De.SandroMaierhofer.POS_Michael.Logic.Ui.Services
{
    public class Printer
    {
        TableVM TableVM = new TableVM();
        List<ProductVM> ListProducts;
        Boolean Bewirtung = false;
        PrinterSettings ps = new PrinterSettings();
        private double Steuern = Properties.Settings.Default.TaxFood;
        private double AHSteuern = Properties.Settings.Default.TaxDrinks;

        public void SetPrinterName()
        {
            if (Properties.Settings.Default.ProductMode)
            {
                //Printer Local in Location
                ps.PrinterName = @"\\192.168.1.215\Printer101";
            }
            else
            {
                //Printer for development test
                ps.PrinterName = @"MX920 WLAN";
            }
     
        }
        public void PrintReceiptForTransaction()
        {
            PrintDocument recordDoc = new PrintDocument();
            recordDoc.DocumentName = "Customer Receipt";
            recordDoc.PrintPage += new PrintPageEventHandler(PrintReceiptPage); // function below
            recordDoc.PrintController = new StandardPrintController(); // hides status dialog popup
                                                                       // Comment if debugging 
            SetPrinterName();


            recordDoc.PrinterSettings = ps;
            try
            {
                recordDoc.Print();
            }
            catch (InvalidPrinterException ie) { MessageBox.Show("Drucker nicht verfügbar: " + ie); }

            // --------------------------------------

            // Uncomment if debugging - shows dialog instead
            //PrintPreviewDialog printPrvDlg = new PrintPreviewDialog();
            //printPrvDlg.Document = recordDoc;
            //printPrvDlg.Width = 1200;
            //printPrvDlg.Height = 800;
            //printPrvDlg.ShowDialog();
            // --------------------------------------
            Bewirtung = false;
            recordDoc.Dispose();
        }

        public void PrintOrderForKitchen(List<ProductVM> lp)
        {
            ListProducts = lp;
            PrintDocument recordDoc = new PrintDocument();
            recordDoc.DocumentName = "Customer Receipt";
            recordDoc.PrintPage += new PrintPageEventHandler(PrintReceiptPageOrder); // function below
            recordDoc.PrintController = new StandardPrintController(); // hides status dialog popup
            SetPrinterName();                                                           // Comment if debugging 
            recordDoc.PrinterSettings = ps;
            try
            {
                recordDoc.Print();
            }
            catch (InvalidPrinterException ie) { MessageBox.Show("Drucker nicht verfügbar: " + ie); }

            // --------------------------------------

            // Uncomment if debugging - shows dialog instead
            //PrintPreviewDialog printPrvDlg = new PrintPreviewDialog();
            //printPrvDlg.Document = recordDoc;
            //printPrvDlg.Width = 1200;
            //printPrvDlg.Height = 800;
            //printPrvDlg.ShowDialog();
            // --------------------------------------
            Bewirtung = false;
            recordDoc.Dispose();
        }

        private void PrintReceiptPage(object sender, PrintPageEventArgs e)
        {
            float x = 10;
            float y = 5;
            float width = 270.0F; // max width I found through trial and error
            float height = 0F;

            Font drawFontArial12Bold = new Font("Arial", 12, System.Drawing.FontStyle.Bold);
            Font drawFontArial10Regular = new Font("Arial", 10, System.Drawing.FontStyle.Regular);
            Font drawFontArial8Regular = new Font("Arial", 8, System.Drawing.FontStyle.Regular);
            Font drawFontArial18Regular = new Font("Arial", 18, System.Drawing.FontStyle.Regular);
            SolidBrush drawBrush = new SolidBrush(Color.Black);

            // Set format of string.
            StringFormat drawFormatCenter = new StringFormat();
            drawFormatCenter.Alignment = StringAlignment.Center;
            StringFormat drawFormatLeft = new StringFormat();
            drawFormatLeft.Alignment = StringAlignment.Near;
            StringFormat drawFormatRight = new StringFormat();
            drawFormatRight.Alignment = StringAlignment.Far;

            // Restauraunt Infos
            string text = "Restaurant Michael";
            e.Graphics.DrawString(text, drawFontArial12Bold, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
            y += e.Graphics.MeasureString(text, drawFontArial12Bold).Height;

            text = "Buchholzer Str 4";
            e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
            y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;

            text = "21271 Hanstedt";
            e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
            y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;

            text = "Tel.: 04184 680 99 20";
            e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
            y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;

            text = "Steuernr.: 15/128/17195";
            e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
            y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;

            text = "***************************************";
            e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
            y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;


            // Bestellung

            text = "RECHNUNG";
            e.Graphics.DrawString(text, drawFontArial18Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
            y += e.Graphics.MeasureString(text, drawFontArial18Regular).Height;

            text = "Rechnungsnr.: "+TableVM.RechnungNr.ToString();
            e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatLeft);
            y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;

            text = TableVM.Name;
            e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatLeft);
            y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;

            text = "Datum " + DateTime.Now.ToString(" MMM dd yyyy,HH:mm");
            e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatLeft);
            y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;

            text = "-------------------------------------------------------";
            e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
            y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;

            foreach (ProductVM p in TableVM.ProductVM)
            {
                if (p.Count > 1)
                {
                    text = new string(p.Product_name.Take(35).ToArray()); 
                    e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatLeft);
                    text = string.Format("{0:N2} ", (p.Price)) + " €";
                    e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatRight);
                    y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;

                    text = "   Anzahl: " + p.Count.ToString();
                    e.Graphics.DrawString(text, drawFontArial8Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatLeft);
                    y += e.Graphics.MeasureString(text, drawFontArial8Regular).Height;
                }
                else
                {
                    text = new string(p.Product_name.Take(35).ToArray());
                    e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatLeft);
                    text = string.Format("{0:N2} ", (p.Price)) + " €";
                    e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatRight);
                    y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
                }


            }

            text = "-------------------------------------------------------";
            e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
            y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;

            text = "Total:";
            e.Graphics.DrawString(text, drawFontArial12Bold, drawBrush, new RectangleF(x, y, width, height), drawFormatLeft);
            text = string.Format("{0:N2} ", TableVM.Sum) + " €";
            e.Graphics.DrawString(text, drawFontArial12Bold, drawBrush, new RectangleF(x, y, width, height), drawFormatRight);
            y += e.Graphics.MeasureString(text, drawFontArial12Bold).Height;
            y += e.Graphics.MeasureString(text, drawFontArial12Bold).Height;

            //Steur
            var netto = TableVM.Sum * Steuern / 100;
            var nettoG = TableVM.Sum * (100 -Steuern) / 100;

            text = "Incl.: "+Steuern+"% MwSt. " + netto + " € (Netto: " + nettoG + " €)";
            e.Graphics.DrawString(text, drawFontArial8Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatLeft);
            y += e.Graphics.MeasureString(text, drawFontArial8Regular).Height;

            text = "------------------------------------------------------";
            e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
            y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
            y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;

            //Bewirtung
            if (Bewirtung)
            {
                text = "***********************************************";
                e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;

                text = "Bewirtungsaufwand-Angaben";
                e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;

                text = "(Par. 4 Abs. 5 Ziff. 2 EstG)";
                e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;

                text = "***********************************************";
                e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;

                text = "Bewirtete Person(en):";
                e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatLeft);
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;

                text = "...............................................................";
                e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;

                text = "-------------------------------------------------------";
                e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;

                text = "Anlaß der Bewirtung: ";
                e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatLeft);
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;

                text = "...............................................................";
                e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatLeft);
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;

                text = "-------------------------------------------------------";
                e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;

                text = "Höhe der Aufwendung:";
                e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatLeft);
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;

                text = "...............................................................";
                e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
                text = "Bei Bewirtung im Restaurant";
                e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatLeft);
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
                text = "in anderen Fällen";
                e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatLeft);
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
                text = "-------------------------------------------------------";
                e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;

                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
                text = "...............................................................";
                e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
                text = "Ort       Datum ";
                e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatLeft);
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;

                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
                text = "...............................................................";
                e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
                text = "Unterschrift";
                e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatLeft);
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
                y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
                Bewirtung = false;
            }


            //Allgemeines

            text = "Vielen Dank für Ihren Besuch";
            e.Graphics.DrawString(text, drawFontArial18Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
            y += e.Graphics.MeasureString(text, drawFontArial18Regular).Height;
            y += e.Graphics.MeasureString(text, drawFontArial18Regular).Height;

        }

        private void PrintReceiptPageOrder(object sender, PrintPageEventArgs e)
        {
            float x = 10;
            float y = 5;
            float width = 270.0F; // max width I found through trial and error
            float height = 0F;

            Font drawFontArial12Bold = new Font("Arial", 12, System.Drawing.FontStyle.Bold);
            Font drawFontArial10Regular = new Font("Arial", 10, System.Drawing.FontStyle.Regular);
            Font drawFontArial8Regular = new Font("Arial", 8, System.Drawing.FontStyle.Regular);
            Font drawFontArial18Regular = new Font("Arial", 18, System.Drawing.FontStyle.Regular);
            SolidBrush drawBrush = new SolidBrush(Color.Black);

            // Set format of string.
            StringFormat drawFormatCenter = new StringFormat();
            drawFormatCenter.Alignment = StringAlignment.Center;
            StringFormat drawFormatLeft = new StringFormat();
            drawFormatLeft.Alignment = StringAlignment.Near;
            StringFormat drawFormatRight = new StringFormat();
            drawFormatRight.Alignment = StringAlignment.Far;


            string text = "Bestellung Küche";
            e.Graphics.DrawString(text, drawFontArial12Bold, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
            y += e.Graphics.MeasureString(text, drawFontArial12Bold).Height;

            // Bestellung

            text = "Bestellnr.: " + TableVM.OrderId;
            e.Graphics.DrawString(text, drawFontArial18Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
            y += e.Graphics.MeasureString(text, drawFontArial18Regular).Height;

            text = TableVM.Name;
            e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatLeft);
            y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;

            text = "Datum " + DateTime.Now.ToString("dddd , MMM dd yyyy,hh:mm");
            e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatLeft);
            y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;

            text = "-------------------------------------------------------";
            e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
            y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;

            foreach (ProductVM p in ListProducts)
            {
                if (p.Count > 1)
                {
                    text = p.Product_name;
                    e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatLeft);
                    text = string.Format("{0:N2} ", (p.Price)) + " €";
                    e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatRight);
                    y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;

                    text = "Anzahl: " + p.Count.ToString();
                    e.Graphics.DrawString(text, drawFontArial8Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatLeft);
                    y += e.Graphics.MeasureString(text, drawFontArial8Regular).Height;
                }
                else
                {
                    text = p.Product_name;
                    e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatLeft);
                    text = string.Format("{0:N2} ", p.Price) + " €";
                    e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatRight);
                    y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
                }

            }

            y += 50;


        }
        public void SetTable(TableVM t)
        {
            TableVM = t;
        }

        public void SetBewirtung()
        {
            Bewirtung = true;
        }

    }
}
