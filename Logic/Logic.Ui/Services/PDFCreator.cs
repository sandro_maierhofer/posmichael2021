﻿using De.SandroMaierhofer.POS_Michael.Logic.Ui.Wrapper;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace De.SandroMaierhofer.POS_Michael.Logic.Ui.Services
{
    public class PDFCreator
    {

        public void CreatePDFListSalesManagement(List<ProductVM> productList, DateTime start, DateTime end)
        {
            var dateTime = DateTime.Today.ToString("dd/MM/yyyy");
            //Find path to create doc
            var exportFolder = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            var docName = "SalesList_" + DateTime.Today.ToString("dd/MM/yyyy").ToString() + ".pdf";
            var exportFile = Path.Combine(exportFolder, docName);

            //Creating PDF
            Document pdfDocument = new Document(PageSize.A4, 20f, 20f, 30f, 30f);
            Console.WriteLine("Created PDF Doc.");

            PdfWriter.GetInstance(pdfDocument, new FileStream(exportFile, FileMode.Create));

            pdfDocument.Open();
            Console.WriteLine("Opened PDF.");

            //Fonts
            Font tableFont = FontFactory.GetFont("Helvetica", 8, Font.BOLD);
            Font font = FontFactory.GetFont("Helvetica", 8);
            Font headerFont = FontFactory.GetFont("Helvetica", 14, Font.BOLD);
            Console.WriteLine("Set font.");


            //Logo

            Assembly assembly = Assembly.GetExecutingAssembly();
            Stream stream = assembly.GetManifestResourceStream("De.SandroMaierhofer.POS_Michael.Logic.Ui.Ressources.cover.png");
            Image logo = iTextSharp.text.Image.GetInstance(stream);

            logo.ScaleToFit(75f, 75f);
            logo.ScaleToFit(75f, 75f);
            logo.SetAbsolutePosition(pdfDocument.PageSize.Width - 20f - 75f, pdfDocument.PageSize.Height - 75f);
            pdfDocument.Add(logo);
            Console.WriteLine("Added Logo.");


            //Headers
            Paragraph header = new Paragraph("Liste der Verkauften Artikel vom "+ start.ToString("dd/MM/yyyy") + " bis "+ end.ToString("dd/MM/yyyy"), headerFont);
            pdfDocument.Add(header);
            Paragraph header2 = new Paragraph("Erstellt am: " + dateTime, font);
            pdfDocument.Add(header2);

            if (productList.Count() != 0)
            {
                //Data table
                PdfPTable dataTable = new PdfPTable(3);  //Size of Table (columns)
                dataTable.SpacingBefore = 40f;
                dataTable.WidthPercentage = 100f;
                Console.WriteLine("Created table.");

                //Table headers
                PdfPCell produktHeader = new PdfPCell(new Phrase("Produktname", tableFont));  //Header for Products column
                dataTable.AddCell(produktHeader);

                PdfPCell tableHeader = new PdfPCell(new Phrase("Preis", tableFont));    //Header for Price column
                dataTable.AddCell(tableHeader);

                PdfPCell anzahlHeader = new PdfPCell(new Phrase("Anzahl", tableFont));    //Header for Count column
                dataTable.AddCell(anzahlHeader);

                var allSum = 0.0;
                //Fill table with card data
                foreach (ProductVM p in productList) //Create one row for every product 
                {

                    dataTable.AddCell(p.Product_name);
                    dataTable.AddCell(string.Format("€{0:N2} Euro", p.Price).ToString());
                    dataTable.AddCell(p.Count.ToString());
                    allSum += p.Price;
                }
                Console.WriteLine("Added every cell.");

                pdfDocument.Add(dataTable);
                Console.WriteLine("Sucessfully added table.");

                Paragraph summe = new Paragraph("Summe: " + string.Format("€{0:N2} Euro", allSum), headerFont);
                pdfDocument.Add(summe);

                pdfDocument.Close();
                Console.WriteLine("Closed PDF.");
            }

        }

        public void CreatePDFListBillManagement(List<BillVM> productList, DateTime start, DateTime end)
        {
            var dateTime = DateTime.Today.ToString("dd/MM/yyyy");
            //Find path to create doc
            var exportFolder = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            var docName = "BillList_" + DateTime.Today.ToString("dd/MM/yyyy").ToString() + ".pdf";
            var exportFile = Path.Combine(exportFolder, docName);

            //Creating PDF
            Document pdfDocument = new Document(PageSize.A4, 20f, 20f, 30f, 30f);
            Console.WriteLine("Created PDF Doc.");

            PdfWriter.GetInstance(pdfDocument, new FileStream(exportFile, FileMode.Create));

            pdfDocument.Open();
            Console.WriteLine("Opened PDF.");

            //Fonts
            Font tableFont = FontFactory.GetFont("Helvetica", 8, Font.BOLD);
            Font font = FontFactory.GetFont("Helvetica", 8);
            Font headerFont = FontFactory.GetFont("Helvetica", 14, Font.BOLD);
            Console.WriteLine("Set font.");


            //Logo

            Assembly assembly = Assembly.GetExecutingAssembly();
            Stream stream = assembly.GetManifestResourceStream("De.SandroMaierhofer.POS_Michael.Logic.Ui.Ressources.cover.png");
            Image logo = iTextSharp.text.Image.GetInstance(stream);

            logo.ScaleToFit(75f, 75f);
            logo.ScaleToFit(75f, 75f);
            logo.SetAbsolutePosition(pdfDocument.PageSize.Width - 20f - 75f, pdfDocument.PageSize.Height - 75f);
            pdfDocument.Add(logo);
            Console.WriteLine("Added Logo.");


            //Headers
            Paragraph header = new Paragraph("Liste der Verkauften Artikel vom " + start.ToString("dd/MM/yyyy") + " bis " + end.ToString("dd/MM/yyyy"), headerFont);
            pdfDocument.Add(header);
            Paragraph header2 = new Paragraph("Erstellt am: " + dateTime, font);
            pdfDocument.Add(header2);

            if (productList.Count() != 0)
            {
                //Data table
                PdfPTable dataTable = new PdfPTable(5);  //Size of Table (columns)
                dataTable.SpacingBefore = 40f;
                dataTable.WidthPercentage = 100f;
                Console.WriteLine("Created table.");

                //Table headers
                PdfPCell billIDHeader = new PdfPCell(new Phrase("Rechnungsnummer", tableFont));  
                dataTable.AddCell(billIDHeader);

                PdfPCell orderHeader = new PdfPCell(new Phrase("Bestellnummer", tableFont));   
                dataTable.AddCell(orderHeader);

                PdfPCell euroHeader = new PdfPCell(new Phrase("Euro", tableFont));    
                dataTable.AddCell(euroHeader);

                PdfPCell dateHeader = new PdfPCell(new Phrase("Datum", tableFont));
                dataTable.AddCell(dateHeader);

                PdfPCell payedHeader = new PdfPCell(new Phrase("Transaktionstyp", tableFont));
                dataTable.AddCell(payedHeader);

                var allSum = 0.0;
                //Fill table with card data
                foreach (BillVM p in productList) //Create one row for every product 
                {

                    dataTable.AddCell(p.ID.ToString());
                    dataTable.AddCell(p.Order_ID.ToString());
                    dataTable.AddCell(string.Format("€{0:N2} Euro", p.Euro).ToString());
                    dataTable.AddCell(p.Order_Date.ToString());
                    switch (p.TransactionTyp)
                    {
                        case 1:
                            dataTable.AddCell("Gutschein");
                            break;
                        case 2:
                            dataTable.AddCell("Karte");
                            break;
                        case 3:
                            dataTable.AddCell("Bar");
                            break;
                        default:
                            dataTable.AddCell("Bar");
                            break;
                    }
                    
                    allSum += p.Euro;
                }
                Console.WriteLine("Added every cell.");

                pdfDocument.Add(dataTable);
                Console.WriteLine("Sucessfully added table.");

                Paragraph summe = new Paragraph("Summe: " + string.Format("€{0:N2} Euro", allSum), headerFont);
                pdfDocument.Add(summe);

                pdfDocument.Close();
                Console.WriteLine("Closed PDF.");
            }

        }
    }
}
