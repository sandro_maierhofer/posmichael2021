﻿using De.SandroMaierhofer.POS_Michael.Logic.Ui.Services;
using De.SandroMaierhofer.POS_Michael.Logic.Ui.Wrapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace De.SandroMaierhofer.POS_Michael.Logic.Ui.ViewModel
{
    public class LoginVM : ViewModelBase
    {
        //Properties
        public String Username { get; set; }
        public RelayCommand<object> LoginButton { get; }
        private MainWindowVM Main;

        public LoginVM(MainWindowVM mainVM)
        {
            this.Main = mainVM;
            LoginButton = new RelayCommand<object>(x => Login(x));
        }

        public void Login(object parameter)
        {
            var passwordBox = parameter as PasswordBox;
            var password = passwordBox.Password;

            if (Username != "" && password != "")
            {
                //1. Encrypt entered password
                var temppw = Cryptography.Encrypt(password);
                //establish connection do database
                Main.SQLHelper.EstablishConnection();
                //get User with username
                try
                {
                    string query = "SELECT * FROM michael.user WHERE username = '" + Username + "'";
                    DataSet ds = Main.SQLHelper.QueryRequest(query);
                    UserVM user = new UserVM();
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        user.ID =(int)dr[0];
                        user.Username = dr[1].ToString();
                        user.Password = dr[2].ToString();
                        user.Role = dr[3].ToString();

                    }

                    if (temppw == user.Password)
                    {
                        Main.User = new UserVM(user);
                        Main.CheckUserPermission();
                        Main.ChangeLoginAndTabControl();
                    }
                    else
                    {
                        MessageBox.Show("Passwort und Benutzername stimmen nicht überein!");
                    }
                }
                catch (Exception e)
                {

                }
                Main.SQLHelper.CloseConnection();
            }
            else
            {
                MessageBox.Show("Bitte Eingaben überprüfen");
            }

        }
    }
}
