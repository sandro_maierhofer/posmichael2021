﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De.SandroMaierhofer.POS_Michael.Logic.Ui.ViewModel
{
    public class ViewModelLocator
    {
        public ViewModelLocator()
        {
            //inizialisize properties and mainVM
            MainVM = new MainWindowVM();

            Login = new LoginVM(MainVM);

            Tables = new TablesWindowVM(MainVM);

            Sale = new SaleVM(MainVM);

            TablesUV1 = new TablesWindowUserControl1VM(MainVM, Tables);

            TablesUV2 = new TablesWindowUserControl2VM(MainVM, Tables);

            ProductCategory = new ProductCategoryUserControlVM(MainVM);

            Product = new ProductsUserControlVM(MainVM);

            Payed = new PayedVM(MainVM);

            Menu = new MenuUserControlVM(MainVM);

            UserManagement = new UserManagementUserControlVM(MainVM);

            BillManagement = new BillManagementUserControlVM(MainVM);

            SalesManage = new SalesManagementUserControlVM(MainVM);

            Settings = new SettingsUserControlVM(MainVM);
        }


        public SaleVM Sale { get; }
        public MainWindowVM MainVM { get; }
        public TablesWindowVM Tables { get; }
        public TablesWindowUserControl1VM TablesUV1 { get; }
        public TablesWindowUserControl2VM TablesUV2 { get; }
        public ProductCategoryUserControlVM ProductCategory { get; }
        public ProductsUserControlVM Product { get; }
        public LoginVM Login { get; }
        public PayedVM Payed { get; }
        public MenuUserControlVM Menu { get; }
        public UserManagementUserControlVM UserManagement { get; }
        public BillManagementUserControlVM BillManagement { get; }
        public SalesManagementUserControlVM SalesManage { get; }
        public SettingsUserControlVM Settings { get; }
    }
}
