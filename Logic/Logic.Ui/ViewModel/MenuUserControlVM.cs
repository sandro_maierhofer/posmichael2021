﻿using De.SandroMaierhofer.POS_Michael.Logic.Ui.Services;
using De.SandroMaierhofer.POS_Michael.Logic.Ui.Wrapper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace De.SandroMaierhofer.POS_Michael.Logic.Ui.ViewModel
{
    public class MenuUserControlVM : ViewModelBase
    {
        #region Properties
        public MainWindowVM Main { get; set; }
        private ObservableCollection<ProductVM> _Products;
        public ObservableCollection<ProductVM> Products
        {
            get { return _Products; }
            set
            {
                _Products = value;
                OnPropertyChanged(nameof(Products));
            }
        }

        private ObservableCollection<ProductVM> _DisplayProducts;
        public ObservableCollection<ProductVM> DisplayProducts
        {
            get { return _DisplayProducts; }
            set
            {
                _DisplayProducts = value;
                OnPropertyChanged(nameof(DisplayProducts));
            }
        }

        private ObservableCollection<ProductCategoryVM> _ProductCategory;
        public ObservableCollection<ProductCategoryVM> ProductCategory
        {
            get { return _ProductCategory; }
            set
            {
                _ProductCategory = value;
                OnPropertyChanged(nameof(ProductCategory));
            }
        }

        private ProductCategoryVM _SelectedValue;
        public ProductCategoryVM SelectedValue
        {
            get { return _SelectedValue; }
            set
            {
                _SelectedValue = value;
                OnPropertyChanged(nameof(SelectedValue));
            }
        }

        private ProductVM _SelectedProductValue;
        public ProductVM SelectedProductValue
        {
            get { return _SelectedProductValue; }
            set
            {
                _SelectedProductValue = value;
                OnPropertyChanged(nameof(SelectedProductValue));
            }
        }

        //Properties to create a new Menu item
        private string _Product_code;
        public string Product_code
        {
            get { return _Product_code; }
            set
            {
                _Product_code = value;
                OnPropertyChanged(nameof(Product_code));
            }
        }
        private string _Product_name;
        public string Product_name
        {
            get { return _Product_name; }
            set
            {
                _Product_name = value;
                OnPropertyChanged(nameof(Product_name));
            }
        }
        private string _Description;
        public string Description
        {
            get { return _Description; }
            set
            {
                _Description = value;
                OnPropertyChanged(nameof(Description));
            }
        }
        private int _Category;
        public int Category
        {
            get { return _Category; }
            set
            {
                _Category = value;
                OnPropertyChanged(nameof(Category));
            }
        }
        private int _Tax;
        public int Tax
        {
            get { return _Tax; }
            set
            {
                _Tax = value;
                OnPropertyChanged(nameof(Tax));
            }
        }
        private double _Price;
        public double Price
        {
            get { return _Price; }
            set
            {
                _Price = value;
                OnPropertyChanged(nameof(Price));
            }
        }
        private int _ID;
        public int ID
        {
            get { return _ID; }
            set
            {
                _ID = value;
                OnPropertyChanged(nameof(ID));
            }
        }

        #endregion
        #region RelayCommands
        public RelayCommand<object> CreateProductButton { get; set; }
        public RelayCommand<object> ChangeProductButton { get; set; }
        public RelayCommand<object> SearchForCategory { get; set; }
        public RelayCommand<object> SetSearch { get; set; }

        public RelayCommand<object> DeleteButton { get; set; }

        #endregion
        public MenuUserControlVM(MainWindowVM main)
        {
            Main = main;
            DisplayProducts = new ObservableCollection<ProductVM>();
            Products = new ObservableCollection<ProductVM>();
            ProductCategory = new ObservableCollection<ProductCategoryVM>();
            SearchProdukt();
            Price = 0;
            CreateProductButton = new RelayCommand<object>(x => CreateProduct());
            ChangeProductButton = new RelayCommand<object>(x => ChangeProduct());
            SearchForCategory = new RelayCommand<object>(x => DisplayProductsAsCategory());
            SetSearch = new RelayCommand<object>(x => SetSelectedItemAsProperties());
            DeleteButton = new RelayCommand<object>(x => DeleteProduct());
        }

        public void SearchProdukt()
        {
            Main.SQLHelper.EstablishConnection();
            DataSet ds = Main.SQLHelper.QueryRequest("SELECT * FROM michael.products");
            Products = new ObservableCollection<ProductVM>();

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                Products.Add(new ProductVM
                {

                    ID = Convert.ToInt16(dr[0]),
                    Product_code = dr[1].ToString(),
                    Product_name = dr[2].ToString(),
                    Description = dr[3].ToString(),
                    Price = Convert.ToDouble(dr[4]),
                    Category = Convert.ToInt16(dr[5]),
                    Image = @dr[6].ToString(),
                    Count = 1


                });
                string a = Products[Products.Count - 1].Image;
                a = a.Replace(@"\\", @"\");
                Products[Products.Count - 1].Image = a;
            }

            ds = Main.SQLHelper.QueryRequest("SELECT * FROM michael.product_category");

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                ProductCategory.Add(new ProductCategoryVM
                {
                    ID = (int)dr[0],
                    Name = dr[1].ToString()
                }) ;
            }

            Main.SQLHelper.CloseConnection();
        }

        private void ChangeProduct()
        {
            Console.WriteLine("ChangeProduct");
            if (SelectedProductValue != null)
            {
                Main.SQLHelper.EstablishConnection();
                string tempPrice = Price.ToString();
                tempPrice = tempPrice.Replace(',', '.');
                string query = "UPDATE `michael`.`products` SET `description` = '"+Description+ "', `product_name` = '" + Product_name+ "', `list_price` = '" + tempPrice + "'  WHERE (`id` = '" + SelectedProductValue.ID+"');";
                Main.SQLHelper.SetQuery(query);

                Main.SQLHelper.CloseConnection();
                SearchProdukt();
                DisplayProductsAsCategory();
            }
        }

        private void DeleteProduct()
        {
            Console.WriteLine("CDeleteProduct");
            if (SelectedProductValue != null)
            {
                Main.SQLHelper.EstablishConnection();
                string tempPrice = Price.ToString();
                tempPrice = tempPrice.Replace(',', '.');
                string query = "DELETE FROM  `michael`.`products`  WHERE (`id` = '" + SelectedProductValue.ID + "');";
                Main.SQLHelper.SetQuery(query);

                Main.SQLHelper.CloseConnection();
                SearchProdukt();
                DisplayProductsAsCategory();
            }
        }

        public void SetSelectedItemAsProperties()
        {
           
            if (SelectedProductValue != null)
            {
                Product_code = SelectedProductValue.Product_code;
                Product_name = SelectedProductValue.Product_name;
                Price = SelectedProductValue.Price;
                Category = SelectedProductValue.Category;
                Tax = SelectedProductValue.Tax;
            }
        }

        private void CreateProduct()
        {
            if(ProductCategory != null && Product_name != null && Product_code != null && Price != 0)
            {
                Main.SQLHelper.EstablishConnection();
                string tempPrice = Price.ToString();
                tempPrice = tempPrice.Replace(",", ".");
                string query = "INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('" + Product_code + "', '" + Product_name + "', '" + Description + "', '" + tempPrice + "', '" + SelectedValue.ID + "');";
                Main.SQLHelper.SetQuery(query);

                Main.SQLHelper.CloseConnection();
                SearchProdukt();
                DisplayProductsAsCategory();
            }
        }

        private void DisplayProductsAsCategory()
        {
            
            if(SelectedValue != null)
            {
                DisplayProducts = new ObservableCollection<ProductVM>();
                foreach (ProductVM pvm in Products)
                {
                    if(pvm.Category == SelectedValue.ID)
                    {
                        DisplayProducts.Add(new ProductVM(pvm));
                    }
                }
            }
        }
    }
}
