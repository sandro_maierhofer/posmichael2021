﻿using De.SandroMaierhofer.POS_Michael.Data.SQLHelper;
using De.SandroMaierhofer.POS_Michael.Logic.Ui.Services;
using De.SandroMaierhofer.POS_Michael.Logic.Ui.Wrapper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace De.SandroMaierhofer.POS_Michael.Logic.Ui.ViewModel
{
    public class MainWindowVM : ViewModelBase
    {
        public SQLHelper SQLHelper;
        public BillWindowVM BillWindow { get; set; }
        private bool _BindCategory;
        public bool BindCategory
        {
            get { return _BindCategory; }
            set
            {

                _BindCategory = value;
                OnPropertyChanged(nameof(BindCategory));
            }
        }
        public ObservableCollection<TableVM> Tables { get; set; }
        public ObservableCollection<ProductVM> ProductSearch { get; set; }
        public TableVM BillTableToCalculate { get; set; }
        //Wird für die Bestellung benötigt, temporärer speicher bis Bestellen gedrückt wird
        private ObservableCollection<ProductVM> _CurrentProducts;
        //Enthält den aktuellen Tisch/Person mit der aktuellen Bestellung
        private TableVM _CurrentTable;
        public bool PayedWindowActive = false;
        public TableVM CurrentTable { get { return _CurrentTable; } set { _CurrentTable = value; OnPropertyChanged(nameof(CurrentTable)); } }
        #region TabControls
        // TabControlVisible BOOLEAN
        private bool _TabControlVisible;
        public bool TabControlVisible
        {
            get { return _TabControlVisible; }
            set
            {
                _TabControlVisible = value;
                OnPropertyChanged(nameof(TabControlVisible));
            }
        }
        private bool _TabOrderControl;
        public bool TabOrderControl
        {
            get { return _TabOrderControl; }
            set
            {
                _TabOrderControl = value;
                OnPropertyChanged(nameof(TabOrderControl));
            }
        }
        private bool _TabUserControl;
        public bool TabUserControl
        {
            get { return _TabUserControl; }
            set
            {
                _TabUserControl = value;
                OnPropertyChanged(nameof(TabUserControl));
            }
        }
        private bool _TabBillControl;
        public bool TabBillControl
        {
            get { return _TabBillControl; }
            set
            {
                _TabBillControl = value;
                OnPropertyChanged(nameof(TabBillControl));
            }
        }
        private bool _TabMenuControl;
        public bool TabMenuControl
        {
            get { return _TabMenuControl; }
            set
            {
                _TabMenuControl = value;
                OnPropertyChanged(nameof(TabMenuControl));
            }
        }
        #endregion
        #region Usermanagement
        //*************Usermanagement**********************
        private UserVM _User { get; set; }
        public UserVM User {
            get { return _User; } 
            set
            {
                _User = value;
                OnPropertyChanged(nameof(User));
            } 
        }
        private bool _LoginVisible;
        public bool LoginVisible
        {
            get { return _LoginVisible; }
            set
            {
                _LoginVisible = value;
                OnPropertyChanged(nameof(LoginVisible));
            }
        }
        private bool _ProductsAreChanged;
        public bool ProductsAreChanged
        {
            get { return _ProductsAreChanged; }
            set
            {
                _ProductsAreChanged = value;
                OnPropertyChanged(nameof(ProductsAreChanged));

            }
        }
        private double _Sum;
        public double Sum
        {
            get { return _Sum; }
            set
            {
                _Sum = value;
                OnPropertyChanged(nameof(Sum));
            }
        }
        #endregion
        //To check if the Bill SPlit is in use
        public bool BillSplitIsActive = false;
        //To check if the Bill SPlit is payed
        public bool BillSplitIsPayed = false;
        public ObservableCollection<ProductVM> CurrentProducts
        {
            get { return _CurrentProducts; }
            set
            {
                ProductsAreChanged = true;
                _CurrentProducts = value;
                OnPropertyChanged("");

            }
        }

        public MainWindowVM()
        {
            ProductsAreChanged = false;
            ProductSearch = new ObservableCollection<ProductVM>();
            Tables = new ObservableCollection<TableVM>();
            SQLHelper = new SQLHelper();
            BillWindow = new BillWindowVM(this);
            BindCategory = true;
            LoginVisible = true;
            TabControlVisible = false;
            CurrentProducts = new ObservableCollection<ProductVM>();
            TabMenuControl = false;
            TabBillControl = false;
            TabUserControl = false;
            TabOrderControl = false;
        }
        public void SetSumTable(TableVM t)
        {
            t.Sum = 0;
            foreach (ProductVM p in t.ProductVM)
            {
                if (p.Count > 0)
                {
                  
                   t.Sum += p.Price;
                    
                }

            }
        }
        public void ChangeLoginAndTabControl()
        {
            if (TabControlVisible)
            {
                TabControlVisible = false;
                LoginVisible = true;
            }
            else
            {
                TabControlVisible = true;
                LoginVisible = false;
            }
        }

        public void CheckUserPermission()
        {
            switch (User.Role)
            {
                case "admin":
                    Console.WriteLine("admin");
                    TabMenuControl = true;
                    TabBillControl = true;
                    TabUserControl = true;
                    TabOrderControl = true;
                    break;
                case "manager":
                    Console.WriteLine("managr");
                    TabMenuControl = true;
                    TabBillControl = true;
                    TabUserControl = true;
                    TabOrderControl = true;
                    break;
                case "service":
                    Console.WriteLine("service");
                    TabMenuControl = false;
                    TabBillControl = false;
                    TabUserControl = false;
                    TabOrderControl = true;
                    break;
                default:
                    TabMenuControl = false;
                    TabBillControl = false;
                    TabUserControl = false;
                    TabOrderControl = true;
                    break;
            }
            Console.WriteLine("sc");
        }

        public ObservableCollection<ProductVM> GetProductsFromOrderID(int ID)
        {
            ObservableCollection<ProductVM> temp = new ObservableCollection<ProductVM>();
            //Um Produkte zu der Bestellung zu kriegen falls vorhanden
            string query = "SELECT * FROM michael.products_orders WHERE `orders_id` = " + ID;
            DataSet orderedProducts = SQLHelper.QueryRequest(query);

            DataSet dsproducts = SQLHelper.QueryRequest("SELECT * FROM michael.products");
            //pV sind alle Produkte der DB gespeichert
            ObservableCollection<ProductVM> pV = new ObservableCollection<ProductVM>();
            foreach (DataRow dr in dsproducts.Tables[0].Rows)
            {

                pV.Add(new ProductVM
                {
                    ID = Convert.ToInt16(dr[0]),
                    Product_code = dr[1].ToString(),
                    Product_name = dr[2].ToString(),
                    Description = dr[3].ToString(),
                    Price = Convert.ToDouble(dr[4]),
                    Image = @dr[6].ToString(),
                    Tax = Convert.ToInt16(dr[7]),
                    Count = 1
                });
            }

            try
            {
                //Für jedes Produkt
                foreach (DataRow dr in orderedProducts.Tables[0].Rows)
                {

                    foreach (ProductVM p in pV)
                    {
                        if (p.ID == Convert.ToInt16(dr[0]))
                        {
                            p.Count = (int)dr[2];
                            temp.Add(p);
                        }
                    }

                }
            }
            catch { }
            return temp;
        }

        public TableVM CreateTableForOrder(int order_ID)
        {
            TableVM t = new TableVM();
            if (order_ID > 0)
            {
                SQLHelper.EstablishConnection();

                //Um die ID von der Bestellung auf dem Tisch zu bekommen, wo der Tisch nicht auf closed ist
                var query = "SELECT * FROM michael.orders WHERE `id` = '" + order_ID + "'";
                DataSet ds = SQLHelper.QueryRequest(query);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    t.OrderId = Convert.ToInt16(dr[0]);
                    t.EmployeeID = Convert.ToInt16(dr[1]);
                    t.Status = Convert.ToInt16(dr[4]);
                    t.Id = Convert.ToInt16(dr[2]);
                }

                //Um Produkte zu der Bestellung zu kriegen falls vorhanden
                query = "SELECT * FROM michael.products_orders WHERE `orders_id` = " + t.OrderId;
                ds = SQLHelper.QueryRequest(query);

                DataSet dsproducts = SQLHelper.QueryRequest("SELECT * FROM michael.products");
                //pV sind alle Produkte der DB gespeichert
                ObservableCollection<ProductVM> pV = new ObservableCollection<ProductVM>();
                foreach (DataRow dr in dsproducts.Tables[0].Rows)
                {

                    pV.Add(new ProductVM
                    {
                        ID = Convert.ToInt16(dr[0]),
                        Product_code = dr[1].ToString(),
                        Product_name = dr[2].ToString(),
                        Description = dr[3].ToString(),
                        Price = Convert.ToDouble(dr[4]),
                        Image = @dr[6].ToString(),
                        Count = 1
                    });
                }

                try
                {
                    //Für jedes Produkt
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {

                        foreach (ProductVM p in pV)
                        {
                            if (p.ID == Convert.ToInt16(dr[0]))
                            {
                                p.Count = (int)dr[2];
                                p.Price *= p.Count;
                                t.ProductVM.Add(p);
                                t.Sum += p.Price;
                            }
                        }

                    }
                }
                catch { }

                SQLHelper.CloseConnection();
            }
            return t;
            
        }

        //NOT NEEDED NOW
        public void CreateOrderSQL(int orderStatus, TableVM table)
        {
            string d = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string orderID = "";
            //Erstellt Bestellung!
            string query = "INSERT INTO michael.orders (`employee_id`, `table`, `order_date`, `order_status`) VALUES ('3', '" + table.Id + "', '" + d + "', '" + orderStatus + "')";
            //Erstellt zu der Bestellung die Verknüpfungen zu den Produkten

            SQLHelper.EstablishConnection();
            SQLHelper.SetQuery(query);

            //Get ID from Order
            query = "SELECT * FROM michael.orders WHERE `table` = " + table.Id + " AND `order_date` = '" + d + "'";
            DataSet ds = SQLHelper.QueryRequest(query);
            foreach (DataRow dr in ds.Tables[0].Rows)
            {

                orderID = dr[0].ToString();
                Console.WriteLine(orderID);
                CurrentTable.OrderId = Convert.ToInt16(dr[0]);
            }
            //Add Products to Order
            query = "";
            foreach (ProductVM p in table.ProductVM)
            {
                query += "INSERT INTO `michael`.`products_orders` (`product_id`, `orders_id`, `number_product`) VALUES ('" + p.ID + "', '" + orderID + "', '" + p.Count + "');";

            }

            SQLHelper.SetQuery(query);
            SQLHelper.CloseConnection();
        }

        public void CreateBillSQL(TableVM table)
        {
            //Rechnung erstellen
            SQLHelper.EstablishConnection();
            string d = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string pro = table.Sum.ToString();
            pro = pro.Replace(@",", ".");
            Console.WriteLine(pro);
            string query = " INSERT INTO `michael`.`bill` (`order_id`, `euro`, `order_date`, `status_id`) VALUES ('" + table.OrderId + "', '" + pro + "', '" + d + "', '1');";
            SQLHelper.SetQuery(query);

            //RechnungsID speichern 
            query = "Select `id` from michael.bill where `order_id` = " + table.OrderId + " and `order_date` =  \"" + d + " \" ;";
            DataSet ds = SQLHelper.QueryRequest(query);
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                Console.WriteLine(dr[0]);
                table.RechnungNr = Convert.ToInt16(dr[0]);
            }

            query = "UPDATE michael.orders SET `bill_id` = " + table.RechnungNr + " WHERE `id` = " + table.OrderId;  //Tisch Status 3
            SQLHelper.SetQuery(query);
            SQLHelper.CloseConnection();
        }


    }
}
