﻿using De.SandroMaierhofer.POS_Michael.Logic.Ui.Services;
using De.SandroMaierhofer.POS_Michael.Logic.Ui.Wrapper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace De.SandroMaierhofer.POS_Michael.Logic.Ui.ViewModel
{
    public class BillManagementUserControlVM : ViewModelBase
    {
        #region Properties
        public MainWindowVM Main { get; set; }

        private string _Sum;
        public string Sum
        {
            get { return _Sum; }
            set
            {
                _Sum = value;
                OnPropertyChanged(nameof(Sum));
            }
        }
        private DateTime _DTPStart;
        public DateTime DTPStart
        {
            get { return _DTPStart; }
            set
            {
                _DTPStart = value;
                OnPropertyChanged(nameof(DTPStart));
            }
        }
        private DateTime _DTPEnd;
        public DateTime DTPEnd
        {
            get { return _DTPEnd; }
            set
            {
                _DTPEnd = value;
                OnPropertyChanged(nameof(DTPEnd));
            }
        }
        private int _TimeStart;
        public int TimeStart
        {
            get { return _TimeStart; }
            set
            {
                _TimeStart = value;
                OnPropertyChanged(nameof(TimeStart));
            }
        }

        private int _TimeEnd;
        public int TimeEnd
        {
            get { return _TimeEnd; }
            set
            {
                _TimeEnd = value;
                OnPropertyChanged(nameof(TimeEnd));
            }
        }

        private List<UserVM> _User;
        public List<UserVM> User
        {
            get { return _User; }
            set
            {
                _User = value;
                OnPropertyChanged(nameof(User));
            }
        }
        private UserVM _SelectedUser;
        public UserVM SelectedUser
        {
            get { return _SelectedUser; }
            set
            {
                _SelectedUser = value;
                OnPropertyChanged(nameof(SelectedUser));
            }
        }
        private BillVM _SelectedBill;
        public BillVM SelectedBill
        {
            get { return _SelectedBill; }
            set
            {
                _SelectedBill = value;
                OnPropertyChanged(nameof(SelectedBill));
            }
        }

        private ObservableCollection<BillVM> _Bill;
        public ObservableCollection<BillVM> Bill
        {
            get { return _Bill; }
            set
            {
                _Bill = value;
                OnPropertyChanged(nameof(Bill));
            }
        }

        private ObservableCollection<BillVM> _BillOutOfHouse;
        public ObservableCollection<BillVM> BillOutOfHouse
        {
            get { return _BillOutOfHouse; }
            set
            {
                _BillOutOfHouse = value;
                OnPropertyChanged(nameof(BillOutOfHouse));
            }
        }
        #endregion
        #region RelayCommands
        public RelayCommand<object> Search { get; set; }
        public RelayCommand<object> PrintBillBut { get; set; }
        public RelayCommand<object> DeleteBut { get; set; }

        #endregion
        public BillManagementUserControlVM(MainWindowVM main)
        {
            Main = main;
            DTPEnd = DateTime.Now;
            DTPStart = DateTime.Now;
            TimeEnd = 18;
            TimeStart = 8;
            Bill = new ObservableCollection<BillVM>();
            Search = new RelayCommand<object>(x => SearchDBForRange());
            DeleteBut = new RelayCommand<object>(x => DeleteBill());
            PrintBillBut = new RelayCommand<object>(x => PrintBill());
            SearchForUser();
        }

        private void PrintBill()
        {
            if(SelectedBill != null)
            {
                TableVM t = Main.CreateTableForOrder(SelectedBill.Order_ID);
                t.RechnungNr = SelectedBill.ID;
                Printer p = new Printer();
                p.SetTable(t);
                p.PrintReceiptForTransaction();

                Console.WriteLine("Drucke Rechnung!" + SelectedBill.Order_ID);
            }
        }

        private void CreateBillDocument()
        {
            PDFCreator pdf = new PDFCreator();
            var list = new List<BillVM>();

            foreach (BillVM p in Bill)
            {
                list.Add(p);
            }

            pdf.CreatePDFListBillManagement(list, DTPStart, DTPEnd);
        }

        private void DeleteBill()
        {
            if (SelectedBill != null)
            {
                Main.SQLHelper.EstablishConnection();
                string query = "DELETE FROM  `michael`.`bill`  WHERE (`id` = '" + SelectedBill.ID + "');";
                Main.SQLHelper.SetQuery(query);
                //Bestellung Löschen?
                MessageBoxResult result = System.Windows.MessageBox.Show("Möchten Sie die Bestellung auch Löschen?", "Print?", MessageBoxButton.YesNo);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        //Alles Löschen
                        query = "DELETE FROM michael.products_orders WHERE orders_id = '"+SelectedBill.Order_ID+"'";
                        Main.SQLHelper.SetQuery(query);
                        query = "DELETE FROM michael.orders WHERE id = '" + SelectedBill.Order_ID + "'";
                        Main.SQLHelper.SetQuery(query);
                        break;
                    case MessageBoxResult.No:
                        query = "UPDATE `michael`.`orders` SET `order_status` = '1' WHERE (`id` = '"+SelectedBill.Order_ID+"');";
                        Main.SQLHelper.SetQuery(query);

                        query = "SELECT * FROM `michael`.`orders` WHERE (`id` = '" + SelectedBill.Order_ID + "');";
                        DataSet ds = Main.SQLHelper.QueryRequest(query);
                        int tableID = 0;
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            tableID = (Convert.ToInt16(dr[2]));
                        }

                        query = "UPDATE `michael`.`tables` SET `status` = '2' WHERE (`id` = '" + tableID + "');";
                        Main.SQLHelper.SetQuery(query);
                        break;

                }

                Main.SQLHelper.CloseConnection();
                SearchDBForRange();
            }
        }

        //new search
        private void SearchDBForRange()
        {
            Bill = new ObservableCollection<BillVM>();
            BillOutOfHouse = new ObservableCollection<BillVM>();
            Main.SQLHelper.EstablishConnection();
            DateTime DTPEndTemp = DTPEnd.AddHours(TimeEnd);
            DateTime DTPStartTemp = DTPStart.AddHours(TimeStart);
            string query = "";
            string queryAH = "";
            if (SelectedUser != null)
            {
                try
                {
                    queryAH = "SELECT id FROM michael.orders WHERE employee_id = '" + SelectedUser.ID + "' && `table` = '28' && orders_status != '1' && orders_status != '2' && order_date > '" + DTPStartTemp.ToString("yyyy-MM-dd HH:mm:ss") + "' && order_date <= '" + DTPEndTemp.ToString("yyyy-MM-dd HH:mm:ss") + "' ";
                    query = "SELECT id FROM michael.orders WHERE employee_id = '" + SelectedUser.ID + "' && `table` != '28' && orders_status != '1' && orders_status != '2' && order_date > '" + DTPStartTemp.ToString("yyyy-MM-dd HH:mm:ss") + "' && order_date <= '" + DTPEndTemp.ToString("yyyy-MM-dd HH:mm:ss") + "' ";
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }

            }
            else
            {
                query = "SELECT id FROM michael.orders WHERE  `table` != '28' && order_date > '" + DTPStartTemp.ToString("yyyy-MM-dd HH:mm:ss") + "' && order_date <= '" + DTPEndTemp.ToString("yyyy-MM-dd HH:mm:ss") + "' ";
                queryAH = "SELECT id FROM michael.orders WHERE `table` = '28' && order_date > '" + DTPStartTemp.ToString("yyyy-MM-dd HH:mm:ss") + "' && order_date <= '" + DTPEndTemp.ToString("yyyy-MM-dd HH:mm:ss") + "' ";
            }

            DataSet ds = Main.SQLHelper.QueryRequest(query);
            DataSet dsAH = Main.SQLHelper.QueryRequest(queryAH);
            //List for queryrequest -> OrderID´s
            List<int> orderIDs = new List<int>();
            List<int> orderIDsAH = new List<int>();

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                orderIDs.Add(Convert.ToInt16(dr[0]));
            }

            if (dsAH != null)
            {
                foreach (DataRow drAH in dsAH.Tables[0].Rows)
                {
                    orderIDsAH.Add(Convert.ToInt16(drAH[0]));
                }
            }


            //Now Search BILLS
            query = "SELECT * FROM michael.bill WHERE order_id IN ('";
            foreach (int a in orderIDs)
            {
                query += a.ToString() + "','";
            }
            query += "');";

            //Now Search BILLSAH
            queryAH = "SELECT * FROM michael.bill WHERE order_id IN ('";
            foreach (int a in orderIDsAH)
            {
                queryAH += a.ToString() + "','";
            }
            queryAH += "');";

            ds = Main.SQLHelper.QueryRequest(query);
            dsAH = Main.SQLHelper.QueryRequest(queryAH);
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                Bill.Add(new BillVM
                {
                    ID = Convert.ToInt16(dr[0]),
                    Order_ID = Convert.ToInt16(dr[1]),
                    Euro = Convert.ToDouble(dr[2]),
                    Order_Date = Convert.ToDateTime(dr[3]),
                    Status = Convert.ToInt16(dr[4]),
                    TransactionTyp = Convert.ToInt16(dr[5])
                });

            }

            foreach (DataRow dr in dsAH.Tables[0].Rows)
            {
                BillOutOfHouse.Add(new BillVM
                {
                    ID = Convert.ToInt16(dr[0]),
                    Order_ID = Convert.ToInt16(dr[1]),
                    Euro = Convert.ToDouble(dr[2]),
                    Order_Date = Convert.ToDateTime(dr[3]),
                    Status = Convert.ToInt16(dr[4]),
                    TransactionTyp = Convert.ToInt16(dr[5])
                });

            }


            CreateBillDocument();
            Main.SQLHelper.CloseConnection();

        }
        private void SearchForUser()
        {
            Main.SQLHelper.EstablishConnection();
            string query = "SELECT * FROM michael.user;";
            DataSet ds = Main.SQLHelper.QueryRequest(query);
            User = new List<UserVM>();
            foreach (DataRow dr in ds.Tables[0].Rows)
            {

                User.Add(new UserVM
                {
                    ID = (int)dr[0],
                    Username = dr[1].ToString()
                });


            }
            Main.SQLHelper.CloseConnection();
        }


    }
}
