﻿using De.SandroMaierhofer.POS_Michael.Logic.Ui.Messages;
using De.SandroMaierhofer.POS_Michael.Logic.Ui.Services;
using De.SandroMaierhofer.POS_Michael.Services.MediatorServiceBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace De.SandroMaierhofer.POS_Michael.Logic.Ui.ViewModel
{
    public class PayedVM : ViewModelBase
    {
        MainWindowVM Main;

        public RelayCommand<object> ButtonClickCash { get; set; }
        public RelayCommand<object> ButtonClickCoupon { get; set; }
        public RelayCommand<object> ButtonClickCreditcard { get; set; }
        public PayedVM(MainWindowVM m)
        {
            Main = m;
            ButtonClickCash = new RelayCommand<object>(x => OnClickButtonCash());
            ButtonClickCoupon = new RelayCommand<object>(x => OnClickButtonCoupon());
            ButtonClickCreditcard = new RelayCommand<object>(x => OnClickButtonCreditcard());
        }

        public void OnClickButtonCash()
        {

            if (Main.BillSplitIsActive)
            {
                SplitIsActive(3);
            }
            else
            {
                CreateBill(3);
            }

            ServiceBus.Instance.Send(new ClosePayedWindow(Main));
        }
        public void OnClickButtonCreditcard()
        {
            if (Main.BillSplitIsActive)
            {
                SplitIsActive(2);
            }
            else
            {
                CreateBill(2);
            }

            ServiceBus.Instance.Send(new ClosePayedWindow(Main));

        }
        public void OnClickButtonCoupon()
        {
            if (Main.BillSplitIsActive)
            {
                SplitIsActive(2);
            }
            else
            {
                CreateBill(2);
            }

            ServiceBus.Instance.Send(new ClosePayedWindow(Main));

        }
        private void CreateBill(int transID)
        {
            Main.SQLHelper.EstablishConnection();
            string query = "UPDATE `michael`.`bill` SET `status_id` = 2 WHERE (`id` = " + Main.CurrentTable.RechnungNr + ");"; //Rechnung Status 2
            Main.SQLHelper.SetQuery(query);
            query = "UPDATE `michael`.`bill` SET `bill_transaction_types_id` = " + transID + " WHERE (`id` = " + Main.CurrentTable.RechnungNr + ");"; //Transation
            Main.SQLHelper.SetQuery(query);
            query = "UPDATE michael.tables SET status = 4 WHERE `id` = " + Main.CurrentTable.Id;  //Tisch Status 4
            Main.SQLHelper.SetQuery(query);
            query = "UPDATE michael.orders SET order_status = 4 WHERE `id` = " + Main.CurrentTable.OrderId;  //Order Status 4
            Main.SQLHelper.SetQuery(query);
            Main.SQLHelper.CloseConnection();
        }

        private void SplitIsActive(int id)
        {
            if (Main.BillSplitIsActive)
            {
                Main.SQLHelper.EstablishConnection();
                string query = "UPDATE `michael`.`bill` SET `status_id` = 2 WHERE (`id` = " + Main.CurrentTable.RechnungNr + ");"; //Rechnung Status 2
                Main.SQLHelper.SetQuery(query);
                query = "UPDATE `michael`.`bill` SET `bill_transaction_types_id` = " + id + " WHERE (`id` = " + Main.CurrentTable.RechnungNr + ");"; //Transation
                Main.SQLHelper.SetQuery(query);
                query = "UPDATE michael.orders SET order_status = 3 WHERE `id` = " + Main.CurrentTable.OrderId;  //Order Status 3
                Main.SQLHelper.SetQuery(query);
                Main.SQLHelper.CloseConnection();

                Main.BillSplitIsPayed = true;
                Main.BillSplitIsActive = false;
                Main.SQLHelper.CloseConnection();
            }
        }
    }
}
