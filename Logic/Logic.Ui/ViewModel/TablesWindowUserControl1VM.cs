﻿using De.SandroMaierhofer.POS_Michael.Logic.Ui.Messages;
using De.SandroMaierhofer.POS_Michael.Logic.Ui.Services;
using De.SandroMaierhofer.POS_Michael.Logic.Ui.Wrapper;
using De.SandroMaierhofer.POS_Michael.Services.MediatorServiceBus;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De.SandroMaierhofer.POS_Michael.Logic.Ui.ViewModel
{
    public class TablesWindowUserControl1VM : ViewModelBase
    {

        public MainWindowVM Main { get; set; }
        private TablesWindowVM parent { get; set; }
        private TableVM _SelectedTable;
        public TableVM SelectedTable
        {
            get { return _SelectedTable; }
            set
            {
                _SelectedTable = value;
                OnPropertyChanged(nameof(SelectedTable));
            }
        }
        public RelayCommand<object> GetTable { get; set; }
        public RelayCommand<object> ReloadTables { get; set; }
        private ObservableCollection<TableVM> _OpenOrderTables { get; set; }
        public ObservableCollection<TableVM> OpenOrderTables
        {
            get { return _OpenOrderTables; }
            set
            {
                _OpenOrderTables = value;
                OnPropertyChanged(nameof(OpenOrderTables));
            }
        }
        public TablesWindowUserControl1VM(MainWindowVM main, TablesWindowVM par)
        {
            Main = main;
            parent = par;
            ReloadTables = new RelayCommand<object>(x => LookForOpenOrderOnTable());
            GetTable = new RelayCommand<object>(x => SetColumnAsTable());
          
        }
        public void LookForOpenOrderOnTable()
        {
            DataSet ds;
            Main.SQLHelper.EstablishConnection();
            switch (Main.User.Role)
            {
                case "manager": 
                    ds = Main.SQLHelper.QueryRequest("SELECT * FROM michael.tables WHERE `status` != 4");
                    break;
                case "service": 
                    ds = Main.SQLHelper.QueryRequest(" SELECT * FROM michael.tables t inner join michael.orders o ON t.id = o.table where o.employee_id = "+Main.User.ID+" AND o.order_status = 1;");
                    break;
                case "admin": 
                    ds = Main.SQLHelper.QueryRequest("SELECT * FROM michael.tables WHERE `status` != 4"); 
                    break;
                default:
                    ds = Main.SQLHelper.QueryRequest(" SELECT * FROM michael.tables t inner join michael.orders o ON t.id = o.table where o.employee_id = "+Main.User.ID+" AND o.order_status = 1;");
                    break;
            }
            
            OpenOrderTables = new ObservableCollection<TableVM>();
            OnPropertyChanged("Obs");
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                bool exist = false;

                foreach (TableVM t in Main.Tables)
                {
                    if (t.Id == (int)dr[0])
                    {
                        exist = true;
                    }
                }

                if (!exist)
                {
                    OpenOrderTables.Add(new TableVM
                    {
                        Id = Convert.ToInt16(dr[0]),
                        Name = dr[1].ToString(),
                        ShortName = dr[2].ToString(),
                        Status = (int)dr[4],
                        Sum = 0
                    });
                }


            }

            Main.SQLHelper.CloseConnection();
        }

        public void SetColumnAsTable()
        {
            if(SelectedTable != null && !Main.Tables.Contains(SelectedTable))
            {
                TableVM t = SelectedTable;
                Main.SQLHelper.EstablishConnection();

                //Um die ID von der Bestellung auf dem Tisch zu bekommen, wo der Tisch nicht auf closed ist
                var query = "SELECT * FROM michael.orders WHERE `table` = " + t.Id + " AND `order_status` != 4 "+ "and `order_date` >= '" +DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd HH:mm:ss") + "'";
                DataSet ds = Main.SQLHelper.QueryRequest(query);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    t.OrderId = Convert.ToInt16(dr[0]);
                    t.EmployeeID = Convert.ToInt16(dr[1]);
                    t.Status = Convert.ToInt16(dr[4]);
                }

                //RechnungsID speichern 
                query = "Select `id` from michael.bill where `order_id` = " + t.OrderId + ";";
                ds = Main.SQLHelper.QueryRequest(query);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    Console.WriteLine(dr[0]);
                   t.RechnungNr = Convert.ToInt16(dr[0]);
                }

                //Um Produkte zu der Bestellung zu kriegen falls vorhanden
                query = "SELECT * FROM michael.products_orders WHERE `orders_id` = " + t.OrderId;
                ds = Main.SQLHelper.QueryRequest(query);

                DataSet dsproducts = Main.SQLHelper.QueryRequest("SELECT * FROM michael.products");
                //pV sind alle Produkte der DB gespeichert
                ObservableCollection<ProductVM> pV = new ObservableCollection<ProductVM>();
                foreach (DataRow dr in dsproducts.Tables[0].Rows)
                {

                    pV.Add(new ProductVM
                    {
                        ID = Convert.ToInt16(dr[0]),
                        Product_code = dr[1].ToString(),
                        Product_name = dr[2].ToString(),
                        Description = dr[3].ToString(),
                        Price = Convert.ToDouble(dr[4]),
                        Image = @dr[6].ToString(),
                        Count = 1
                    });
                }

                try
                {
                    //Für jedes Produkt
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {

                        foreach (ProductVM p in pV)
                        {
                            if (p.ID == Convert.ToInt16(dr[0]))
                            {
                                p.Count = (int)dr[2];
                                p.Price *= p.Count;
                                t.ProductVM.Add(p);
                                t.Sum += p.Price;
                            }
                        }

                    }
                }
                catch { }

                Main.SQLHelper.CloseConnection();

                Main.Tables.Add(t);
                Main.CurrentTable = t;
                Main.CurrentProducts = new ObservableCollection<ProductVM>(Main.CurrentTable.ProductVM);
                Main.Sum = Main.CurrentTable.Sum;
                Main.ProductsAreChanged = false;
                LookForOpenOrderOnTable();
                ServiceBus.Instance.Send(new CloseTablesWindow("Close"));
            }
            
        }
    }
}
