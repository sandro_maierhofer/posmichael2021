﻿using De.SandroMaierhofer.POS_Michael.Logic.Ui.Messages;
using De.SandroMaierhofer.POS_Michael.Logic.Ui.Services;
using De.SandroMaierhofer.POS_Michael.Logic.Ui.Wrapper;
using De.SandroMaierhofer.POS_Michael.Services.MediatorServiceBus;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace De.SandroMaierhofer.POS_Michael.Logic.Ui.ViewModel
{
    public class TablesWindowUserControl2VM : ViewModelBase
    {
        MainWindowVM Main;
        private TablesWindowVM parent { get; set; }
        public ObservableCollection<TableVM> _EmptyTables { get; set; }
        public ObservableCollection<TableVM> EmptyTables { get { return _EmptyTables; } set { _EmptyTables = value; OnPropertyChanged(); } }

        public RelayCommand<object> SetTable { get; set; }
        public RelayCommand<object> ReloadButton { get; set; }

        public TablesWindowUserControl2VM(MainWindowVM main, TablesWindowVM par )
        {
            Main = main;
            parent = par;
            ReloadButton = new RelayCommand<object>(x => OpenEmptyTables());
            SetTable = new RelayCommand<object>(x => CreateNewOrderOnTable(x));
            OpenEmptyTables();
        }

        public void OpenEmptyTables()
        {
            Main.SQLHelper.EstablishConnection();
            DataSet ds = Main.SQLHelper.QueryRequest("SELECT * FROM michael.tables WHERE `status` = 4");
            EmptyTables = new ObservableCollection<TableVM>();
            OnPropertyChanged("OBC");
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                EmptyTables.Add(new TableVM
                {
                    Id = (int)dr[0],
                    Name = dr[1].ToString(),
                    ShortName = dr[2].ToString(),
                    Status = 1

                });

            }

            Main.SQLHelper.CloseConnection();
            parent.SwitchView = parent.SwitchView;
        }

        public void CreateNewOrderOnTable(object o)
        {
            TableVM t = (TableVM)o;
            string query = "UPDATE michael.tables SET status = 1 WHERE `id` = " + t.Id;           //Tisch Status 1
            Main.SQLHelper.EstablishConnection();
            Main.SQLHelper.SetQuery(query);
            Main.SQLHelper.CloseConnection();
            t.ProductVM = new ObservableCollection<ProductVM>();
            Main.Tables.Add(t);
            if (Main.CurrentTable == null)
            {
                Main.CurrentTable = t;
                Main.CurrentProducts = Main.CurrentTable.ProductVM;
            }
            OpenEmptyTables();
            ServiceBus.Instance.Send(new CloseTablesWindow("Close"));
        }
    }
}
