﻿using De.SandroMaierhofer.POS_Michael.Logic.Ui.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De.SandroMaierhofer.POS_Michael.Logic.Ui.ViewModel
{
    public class SettingsUserControlVM : ViewModelBase
    {
        //Properties
        #region Properties
        private string _Taxnbr = Properties.Settings.Default.Taxnumber;
        public string Taxnbr
        {
            get { return _Taxnbr; }
            set
            {
                _Taxnbr = value;
                Properties.Settings.Default.Taxnumber = value;
                Console.WriteLine("Taxnumber changed ");
                OnPropertyChanged(nameof(Taxnbr));
                Properties.Settings.Default.Save();
            }
        }

        private string _RestName = Properties.Settings.Default.Restaurantname;
        public string RestName
        {
            get { return _RestName; }
            set
            {
                _RestName = value;
                Properties.Settings.Default.Restaurantname = value;
                Console.WriteLine("Restaurant Name changed ");
                OnPropertyChanged(nameof(_RestName));
                Properties.Settings.Default.Save();
            }
        }

        private bool _ProductMode = Properties.Settings.Default.ProductMode;
        public bool ProductMode
        {
            get { return _ProductMode; }
            set
            {
                _ProductMode = value;
                Properties.Settings.Default.ProductMode = value;
                Console.WriteLine("ProductMode changed ");
                OnPropertyChanged(nameof(_ProductMode));
                Properties.Settings.Default.Save();
            }
        }

        private string _PrinterNameA4 = Properties.Settings.Default.PrinterNameA4;
        public string PrinterNameA4
        {
            get { return _PrinterNameA4; }
            set
            {
                _PrinterNameA4 = value;
                Properties.Settings.Default.PrinterNameA4 = value;
                Console.WriteLine("Printer Name from A4 changed");
                OnPropertyChanged(nameof(_PrinterNameA4));
                Properties.Settings.Default.Save();
            }
        }

        private string _PrinterNameBill = Properties.Settings.Default.PrinterNameBill;
        public string PrinterNameBill
        {
            get { return _PrinterNameBill; }
            set
            {
                _PrinterNameBill = value;
                Console.WriteLine("Printer Name from Bill´s changed");
                OnPropertyChanged(nameof(_PrinterNameBill));
                Properties.Settings.Default.PrinterNameBill = value;
                Properties.Settings.Default.Save();
            }
        }


        private int _TaxFood= Properties.Settings.Default.TaxFood;
        public int TaxFood
        {
            get { return _TaxFood; }
            set
            {
                _TaxFood = value;
                Properties.Settings.Default.TaxFood = value;
                Console.WriteLine("Tax for Food changed");
                OnPropertyChanged(nameof(_TaxFood));
                Properties.Settings.Default.Save();
            }
        }

        private int _TaxDrink = Properties.Settings.Default.TaxDrinks;
        public int TaxDrink
        {
            get { return _TaxDrink; }
            set
            {
                _TaxDrink = value;
                Properties.Settings.Default.TaxDrinks = value;
                Console.WriteLine("Tax for Drinks changed");
                OnPropertyChanged(nameof(_TaxDrink));
                Properties.Settings.Default.Save();
            }
        }

        public RelayCommand<object> saveBtn { get; set; }

        public MainWindowVM main;

        #endregion

        public SettingsUserControlVM(MainWindowVM m)
        {
            main = m;
            saveBtn =  new RelayCommand<object>(x => Save());
        }

        private void Save()
        {
            Properties.Settings.Default.Save();
        }
    }
}
