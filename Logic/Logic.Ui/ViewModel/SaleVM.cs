﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using De.SandroMaierhofer.POS_Michael.Logic.Ui.Messages;
using De.SandroMaierhofer.POS_Michael.Logic.Ui.Services;
using De.SandroMaierhofer.POS_Michael.Logic.Ui.Wrapper;
using De.SandroMaierhofer.POS_Michael.Services.MediatorServiceBus;


namespace De.SandroMaierhofer.POS_Michael.Logic.Ui.ViewModel
{
    public class SaleVM : ViewModelBase
    {
        public MainWindowVM mainVM { get; set; }
        private Printer Printer = new Printer();
        #region Propperties

        //Toast Property
        private string _FadeMessage;
        public string FadeMessage
        {
            get { return _FadeMessage; }
            set
            {
                _FadeMessage = value;
                OnPropertyChanged(nameof(FadeMessage));
            }
        }
        //Observable Collection für die Produkte die aus der Datenbank gelesen werden
        public ObservableCollection<ProductVM> Products { get; }
        //public ObservableCollection<ProductVM> SearchProducts { get; set; }

        //Startet die Verbindung zum SQL Server und führt einen befehl aus
        //Gibt das aufgewählte Produkt and das SaleVM über
        public RelayCommand<object> SendCurrentItem { get; set; }
        public RelayCommand<object> PrintOrder { get; set; }
        //Fügt zum Testen ein Produkt in die Observable Collection hinzu
        public RelayCommand<object> ButtonBack { get; set; }
        public RelayCommand<object> ButtonAbort { get; set; }
        //Plus Button welcher einen Tisch hinzufügt
        public RelayCommand<object> AddNewTableButton { get; set; }
        public RelayCommand<object> DeleteCurrentTableButton { get; set; }
        public RelayCommand<object> ChangeCurrentTableButton { get; set; }
        public RelayCommand<object> OrderButton { get; set; }
        public RelayCommand<object> DiscountButton { get; set; }
        public RelayCommand<object> BillButton { get; set; }
        public RelayCommand<object> PayedButton { get; set; }
        public RelayCommand<object> BillSplitButton { get; set; }
        public RelayCommand<object> MergeWithOtherTable { get; set; }
        public RelayCommand<object> MinusButton { get; set; }
        public RelayCommand<object> PlusButton { get; set; }
        #endregion
        public SaleVM(MainWindowVM mainVM)
        {
            this.mainVM = mainVM;
            Products = new ObservableCollection<ProductVM>();
            //SearchProducts = new ObservableCollection<ProductVM>();
            SendCurrentItem = new RelayCommand<object>(x => addItemToList(x));
            ButtonBack = new RelayCommand<object>(x => Back());
            ButtonAbort = new RelayCommand<object>(x => Abort());
            DiscountButton = new RelayCommand<object>(x => Discout());
            PrintOrder = new RelayCommand<object>(x => PrintOrderForKitchen());
            OrderButton = new RelayCommand<object>(x => Order());
            BillButton = new RelayCommand<object>(x => Bill());
            BillSplitButton = new RelayCommand<object>(x => BillSplit());
            PayedButton = new RelayCommand<object>(x => Payed());
            AddNewTableButton = new RelayCommand<object>(x => AddNewTable());
            DeleteCurrentTableButton = new RelayCommand<object>(x => DeleteCurrentTable());
            ChangeCurrentTableButton = new RelayCommand<object>(x => ChangeCurrentTableInView(x));
            MergeWithOtherTable = new RelayCommand<object>(x => MergeTablesFunction());
            MinusButton = new RelayCommand<object>(x => ChangeCountProduct(1, x));
            PlusButton = new RelayCommand<object>(x => ChangeCountProduct(0, x));

        }

        public void Abort()
        {
            mainVM.CurrentProducts = new ObservableCollection<ProductVM>(mainVM.CurrentTable.ProductVM);
            mainVM.SetSumTable(mainVM.CurrentTable);
            mainVM.ProductsAreChanged = false;
            mainVM.Sum = 0;
            FadeMessage = "Änderungen wurden rückgangig gemacht.";
        }
        private void ChangeCountProduct(int type, object o)
        {
            ProductVM tempP;
            if (type == 0)
            {
                //Plus
                try
                {
                    tempP = (ProductVM)o;
                    Console.WriteLine("PLUS");
                    tempP.Price += tempP.Price / tempP.Count;
                    tempP.Count++;
                    mainVM.Sum += tempP.Price / tempP.Count;
                    mainVM.ProductsAreChanged = true;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception" + e);
                }

            }
            else if (type == 1)
            {
                //Minus

                try
                {
                    tempP = (ProductVM)o;
                    foreach (ProductVM p in mainVM.CurrentProducts)
                    {
                        if (p.Product_name == tempP.Product_name)
                        {
                            if (tempP.Count > 0)
                            {
                                Console.WriteLine("Minus");
                                mainVM.Sum -= (tempP.Price / tempP.Count);
                                tempP.Price -= (tempP.Price / tempP.Count);

                                tempP.Count--;
                                if (tempP.Count == 0)
                                {
                                    Console.WriteLine("DELETE");
                                    mainVM.CurrentProducts.Remove(tempP);
                                    break;
                                }
                                mainVM.ProductsAreChanged = true;
                            }
                            break;
                        }
                    }


                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception" + e);
                }

            }
        }
        public void MergeTablesFunction()
        {
            //TO DO
        }
        public void Discout()
        {
            mainVM.CurrentTable.ProductVM.Add(new ProductVM
            {
                Product_name = "Rabatt 10%",
                Count = 1

            });
            mainVM.Sum -= mainVM.Sum * 0.1;
        }
        public void Order()
        {
            if (mainVM.CurrentProducts.Count > 0 && mainVM.ProductsAreChanged)
            {
                if (mainVM.CurrentTable.Status == 1)
                {
                    try
                    {
                        string d = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                        string orderID = "";
                        //Erstellt Bestellung!
                        string query = "INSERT INTO michael.orders (`employee_id`, `table`, `order_date`, `order_status`) VALUES ('" + mainVM.User.ID + "', '" + mainVM.CurrentTable.Id + "', '" + d + "', '1')";
                        //Erstellt zu der Bestellung die Verknüpfungen zu den Produkten

                        mainVM.SQLHelper.EstablishConnection();
                        mainVM.SQLHelper.SetQuery(query);

                        //Ändere Status vom Tisch
                        query = "UPDATE michael.tables SET status = 2 WHERE `id` = " + mainVM.CurrentTable.Id;            //Tisch Status 2
                        mainVM.SQLHelper.SetQuery(query);

                        //Get ID from Order
                        query = "SELECT * FROM michael.orders WHERE `table` = " + mainVM.CurrentTable.Id + " AND `order_date` = '" + d + "'";
                        DataSet ds = mainVM.SQLHelper.QueryRequest(query);
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {

                            orderID = dr[0].ToString();
                            Console.WriteLine(orderID);
                            mainVM.CurrentTable.OrderId = Convert.ToInt16(dr[0]);
                        }
                        //Add Products to Order
                        query = "";
                        foreach (ProductVM p in mainVM.CurrentProducts)
                        {
                            query += "INSERT INTO `michael`.`products_orders` (`product_id`, `orders_id`, `number_product`) VALUES ('" + p.ID + "', '" + orderID + "', '" + p.Count + "');";

                        }

                        mainVM.SQLHelper.SetQuery(query);

                        // mainVM.CurrentTable.Status = 2;     //Temp Tisch Status 2
                        mainVM.SQLHelper.CloseConnection();
                    }
                    catch (Exception e)
                    {
                        System.Windows.MessageBox.Show(e.Message);
                    }

                }
                else//wenn bestellung vorhanden in db
                {
                    try
                    {
                        mainVM.SQLHelper.EstablishConnection();

                        //get products from order
                        string query = "SELECT * FROM michael.products_orders WHERE `orders_id` = " + mainVM.CurrentTable.OrderId;
                        DataSet ds = mainVM.SQLHelper.QueryRequest(query); //All ordered products in database from this order (proID, orderID,count)

                        //vergleiche ds mit pV
                        foreach (ProductVM pro in mainVM.CurrentTable.ProductVM)
                        {
                            bool isPresent = false;
                            foreach (DataRow d in ds.Tables[0].Rows)
                            {
                                if (pro.ID == Convert.ToInt16(d[0]))
                                {
                                    if (pro.Count > (int)d[2])
                                    {

                                        isPresent = true;
                                        query = "UPDATE michael.products_orders SET number_product = " + pro.Count + " WHERE (`product_id` = '" + pro.ID + "') and (`orders_id` = '" + mainVM.CurrentTable.OrderId + "');";
                                        mainVM.SQLHelper.SetQuery(query);
                                    }
                                    else
                                    {
                                        isPresent = true;
                                    }
                                }

                            }
                            if (!isPresent)
                            {
                                query = "INSERT INTO michael.products_orders  (`product_id`, `orders_id`, `number_product`) VALUES ('" + pro.ID + "', '" + mainVM.CurrentTable.OrderId + "', '" + pro.Count + "');";
                                mainVM.SQLHelper.SetQuery(query);
                            }

                        }
                        mainVM.SQLHelper.CloseConnection();
                    }
                    catch (Exception e)
                    {
                        System.Windows.MessageBox.Show(e.Message);
                    }

                }
                mainVM.ProductsAreChanged = false;
                FadeMessage = "Bestellung wurde gesendet!";
            }
            else
            {
                FadeMessage = "Keine Änderungen!";
            }
            
        }
        public void Bill()
        {
            try
            {
                if (mainVM.CurrentTable != null && !mainVM.ProductsAreChanged && mainVM.CurrentTable.Status == 1)
                {

                    if (mainVM.CurrentTable.OrderId != 0)
                    {
                        //Rechnung erstellen
                        mainVM.SQLHelper.EstablishConnection();
                        string d = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                        mainVM.SetSumTable(mainVM.CurrentTable);
                        string pro = mainVM.CurrentTable.Sum.ToString();
                        pro = pro.Replace(@",", ".");
                        Console.WriteLine(pro);
                        string query = " INSERT INTO `michael`.`bill` (`order_id`, `euro`, `order_date`, `status_id`) VALUES ('" + mainVM.CurrentTable.OrderId + "', '" + pro + "', '" + d + "', '1');";
                        mainVM.SQLHelper.SetQuery(query);

                        //RechnungsID speichern 
                        query = "Select `id` from michael.bill where `order_id` = " + mainVM.CurrentTable.OrderId + " and `order_date` =  \"" + d + " \" ;";
                        DataSet ds = mainVM.SQLHelper.QueryRequest(query);
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            Console.WriteLine(dr[0]);
                            mainVM.CurrentTable.RechnungNr = Convert.ToInt16(dr[0]);
                        }

                        //Rechnung_Produkte erstellen in DB
                        foreach (ProductVM pvm in mainVM.CurrentProducts)
                        {
                            string tempPrice = pvm.Price.ToString();
                            tempPrice = tempPrice.Replace(",", ".");
                            query = " INSERT INTO `michael`.`bill_items` (`bill_id`, `product_name`, `price`, `tax`,`amount`) VALUES ('" + mainVM.CurrentTable.RechnungNr + "', '" + pvm.Product_name + "', '" + tempPrice + "', '" + pvm.Tax + "', '" + pvm.Count + "');";
                            mainVM.SQLHelper.SetQuery(query);
                        }

                        //Rechnung Drucken
                        MessageBoxResult result = System.Windows.MessageBox.Show("Benötigen Sie eine Rechnung?", "Print?", MessageBoxButton.YesNo);
                        switch (result)
                        {
                            case MessageBoxResult.Yes:
                                //Rechnung Drucken
                                Printer = new Printer();
                                mainVM.SetSumTable(mainVM.CurrentTable);
                                Printer.SetTable(mainVM.CurrentTable);
                                Printer.PrintReceiptForTransaction();
                                break;
                            case MessageBoxResult.No:

                                break;

                        }
                        //Order Status ändern                
                        query = "UPDATE `michael`.`orders` SET `order_status` = '2' WHERE (`id` = '" + mainVM.CurrentTable.OrderId + "');";  //Order Status 2
                        mainVM.SQLHelper.SetQuery(query);
                        mainVM.CurrentTable.Status = 2;
                        FadeMessage = "Rechnung wurde erstellt!";

                        //Table Status ändern                
                        query = "UPDATE michael.tables SET status = 3 WHERE `id` = " + mainVM.CurrentTable.Id;  //Tisch Status 3
                        mainVM.SQLHelper.SetQuery(query);
                        mainVM.SQLHelper.CloseConnection();
                        FadeMessage = "Rechnung wurde erstellt!";
                    }
                    else
                    {
                        FadeMessage = "Keine Gültige Bestellung!";
                    }

                }
                else
                {
                    FadeMessage = "Überprüfe Bestellstatus!";
                }
            }
            catch (Exception e)
            {
                System.Windows.MessageBox.Show(e.Message);
            }


        }
        public void Payed()
        {

            if (mainVM.CurrentTable != null)
            {
                if (mainVM.CurrentTable.Status == 2 && mainVM.CurrentTable.OrderId != 0)
                {
                    ServiceBus.Instance.Send(new OpenPayedWIndow(mainVM));
                }
                else { FadeMessage = "Bestellung und Rechnungs Status prüfen! "; }

            }
            else { FadeMessage = "Keine Gültige Aktion"; }

        }
        public void addItemToList(object a)
        {
            ProductVM p = (ProductVM)a;
            mainVM.CurrentTable.ProductVM.Add(p);

        }
        public void Back()
        {
            mainVM.BindCategory = true;
        }

        public void AddNewTable()
        {

            ServiceBus.Instance.Send(new OpenTablesWindow("opennewtablewindow"));

        }

        public void BillSplit()
        {
            if (mainVM.CurrentTable.OrderId != 0 && mainVM.CurrentTable.RechnungNr == 0)
            {
                //Temp Tisch für den SPlit erstellen
                mainVM.BillTableToCalculate = new TableVM(mainVM.CurrentTable);
                mainVM.BillWindow.SetTable(mainVM.BillTableToCalculate);
                mainVM.BillSplitIsActive = true;
                ServiceBus.Instance.Send(new OpenBillWindow(mainVM));

            }


        }

        public void ChangeCurrentTableInView(object table)
        {
            mainVM.Sum = 0;
            mainVM.CurrentTable = (TableVM)table;
            mainVM.CurrentProducts = mainVM.CurrentTable.ProductVM;
            mainVM.SetSumTable(mainVM.CurrentTable);
            mainVM.Sum = mainVM.CurrentTable.Sum;

        }
        public void DeleteCurrentTable()
        {
            if (mainVM.CurrentTable != null)
            {
                if (mainVM.CurrentProducts.Count == 0 && mainVM.CurrentTable.ProductVM.Count == 0)
                {
                    Console.WriteLine("Tich Leer");
                    mainVM.SQLHelper.EstablishConnection();
                    string q = "UPDATE michael.tables SET status = 4 WHERE `id` = " + mainVM.CurrentTable.Id;
                    mainVM.SQLHelper.SetQuery(q);
                }

                mainVM.Tables.Remove(mainVM.CurrentTable);
                mainVM.CurrentTable = null;
                mainVM.CurrentProducts = new ObservableCollection<ProductVM>();
                mainVM.Sum = 0;
                mainVM.SQLHelper.CloseConnection();
                OnPropertyChanged("CurrentTable");
            }

        }
        public void AddProducts()
        {

            mainVM.SQLHelper.EstablishConnection();
            DataSet ds = mainVM.SQLHelper.QueryRequest("SELECT * FROM michael.products");


            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                Products.Add(new ProductVM
                {
                    ID = Convert.ToInt16(dr[0]),
                    Product_code = dr[1].ToString(),
                    Product_name = dr[2].ToString(),
                    Description = dr[3].ToString(),
                    Price = Convert.ToDouble(dr[4]),
                    Tax = Convert.ToInt16(dr[7]),
                    Image = @dr[6].ToString(),
                    Count = 1
                });
                string a = Products[Products.Count - 1].Image;
                a = a.Replace(@"\\", @"\");
                Products[Products.Count - 1].Image = a;
            }
            mainVM.SQLHelper.CloseConnection();
        }
        public void PrintOrderForKitchen()
        {
            if (mainVM.CurrentTable.OrderId != 0)
            {
                List<ProductVM> products = new List<ProductVM>();

                foreach (ProductVM p in mainVM.CurrentTable.ProductVM)
                {
                    if (p.Category < 13)
                    {
                        products.Add(p);
                    }

                }
                Printer = new Printer();
                Printer.SetTable(mainVM.CurrentTable);
                Printer.PrintOrderForKitchen(products);
            }


        }
    }
}
