﻿using De.SandroMaierhofer.POS_Michael.Logic.Ui.Services;
using De.SandroMaierhofer.POS_Michael.Logic.Ui.Wrapper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace De.SandroMaierhofer.POS_Michael.Logic.Ui.ViewModel
{
    public class UserManagementUserControlVM : ViewModelBase
    {
        #region Properties
        public MainWindowVM Main { get; set; }
        public UserVM SelectedUser;
        private ObservableCollection<UserVM> _User;
        
        private string _Username;
        public string Username
        {
            get { return _Username; }
            set
            {
                _Username = value;
                OnPropertyChanged(nameof(_Username));
            }
        }
        private string _Password;
        public string Password
        {
            get { return _Password; }
            set
            {
                _Password = value;
                OnPropertyChanged(nameof(_Password));
            }
        }
        private string _Firstname;
        public string Firstname
        {
            get { return _Firstname; }
            set
            {
                _Firstname = value;
                OnPropertyChanged(nameof(_Firstname));
            }
        }
        private string _Lastname;
        public string Lastname
        {
            get { return _Lastname; }
            set
            {
                _Lastname = value;
                OnPropertyChanged(nameof(_Lastname));
            }
        }
        private string _Street;
        public string Street
        {
            get { return _Street; }
            set
            {
                _Street = value;
                OnPropertyChanged(nameof(_Street));
            }
        }
        private string _Mail;
        public string Mail
        {
            get { return _Mail; }
            set
            {
                _Mail = value;
                OnPropertyChanged(nameof(_Mail));
            }
        }
        private string _JobTitle;
        public string JobTitle
        {
            get { return _JobTitle; }
            set
            {
                _JobTitle = value;
                OnPropertyChanged(nameof(_JobTitle));
            }
        }
        private string _City;
        public string City
        {
            get { return _City; }
            set
            {
                _City = value;
                OnPropertyChanged(nameof(_City));
            }
        }
        private string _StateProvince;
        public string StateProvince
        {
            get { return _StateProvince; }
            set
            {
                _StateProvince = value;
                OnPropertyChanged(nameof(_StateProvince));
            }
        }
        private string _PostalZip;
        public string PostalZip
        {
            get { return _PostalZip; }
            set
            {
                _PostalZip = value;
                OnPropertyChanged(nameof(_PostalZip));
            }
        }
        private string _Note;
        public string Note
        {
            get { return _Note; }
            set
            {
                _Note = value;
                OnPropertyChanged(nameof(_Note));
            }
        }

        private string _Role;
        public string Role
        {
            get { return _Role; }
            set
            {
                _Role = value;
                OnPropertyChanged(nameof(_Role));
            }
        }
        private List<string> _Roles;
        public List<string> Roles
        {
            get { return _Roles; }
            set
            {
                _Roles = value;
                OnPropertyChanged(nameof(Roles));
            }
        }
        public ObservableCollection<UserVM> User
        {
            get { return _User; }
            set
            {
                _User = value;
                OnPropertyChanged(nameof(User));
            }
        }
        #endregion
        #region RelayCommand
        public RelayCommand<object> GetUserButton { get; set; }
        public RelayCommand<object> SetUserFromTableClick{ get; set; }
        public RelayCommand<object> CreateUserCommand { get; set; }
        #endregion
        public UserManagementUserControlVM(MainWindowVM main)
        {
            Main = main;
            User = new ObservableCollection<UserVM>();
            GetUserButton = new RelayCommand<object>(x => GetUserFromDB());
            SetUserFromTableClick = new RelayCommand<object>(x => SetSelectedUserAsUser(x));
            CreateUserCommand = new RelayCommand<object>(x => CreateUser());
            Roles = new List<string> { "service", "admin", "manager" };
            GetUserFromDB();
        }

        public void SetSelectedUserAsUser(object u)
        {

            SelectedUser = (UserVM)u;
        }

        public void GetUserFromDB()
        {
            Main.SQLHelper.EstablishConnection();
            string query = "SELECT * FROM michael.user";
            DataSet ds = Main.SQLHelper.QueryRequest(query);
            User = new ObservableCollection<UserVM>();
            UserVM user = new UserVM();
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                user = new UserVM();
                user.ID = (int)dr[0];
                user.Username = dr[1].ToString();
                user.Password = dr[2].ToString();
                user.Role = dr[3].ToString();
                User.Add(user);
            }
            Main.SQLHelper.CloseConnection();
        }

        public void CreateUser()
        {
            if(Firstname!= null && Lastname!= null &&Password != null && Role != null && JobTitle != null)
            {
                Main.SQLHelper.EstablishConnection();

                var temppw = Cryptography.Encrypt(Password);
                var tempEmpNr = "";

                string query = "INSERT INTO `michael`.`employees` (`last_name`, `first_name`, `job_title`, `address`, `city`, `state_province`, `zip_postal_code`, `notes`) VALUES ('" + Lastname + "', '" + Firstname + "', '" + JobTitle + "', '" + Street + "', '" + City + "', '" + StateProvince + "', '" + PostalZip + "', '" + Note + "');";
                Main.SQLHelper.SetQuery(query);

                query = "SELECT id FROM michael.employees WHERE first_name = '" + Firstname + "' AND last_name = '" + Lastname + "'";
                DataSet ds = Main.SQLHelper.QueryRequest(query);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    tempEmpNr = dr[0].ToString();
                }
                Username =  (Firstname +"_"+ Lastname).ToLower();
                query = "INSERT INTO `michael`.`user` (`username`, `password`, `role`, `employee_id`, `status`) VALUES ('" + Username + "', '" + temppw + "' , '" + Role + "', '" + tempEmpNr + "', '1');";
                Main.SQLHelper.SetQuery(query);
                Main.SQLHelper.CloseConnection();
            }
            else
            {
                MessageBox.Show("Bitte Eingaben überprüfen!");
            }
            
        }
    }
}
