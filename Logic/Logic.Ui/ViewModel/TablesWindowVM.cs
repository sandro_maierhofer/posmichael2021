﻿using De.SandroMaierhofer.POS_Michael.Logic.Ui.Services;
using De.SandroMaierhofer.POS_Michael.Logic.Ui.Wrapper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De.SandroMaierhofer.POS_Michael.Logic.Ui.ViewModel
{
    public class TablesWindowVM : ViewModelBase
    {
        /*Klasse wird benutzt um alle Tische einmal inizial zu erstellen
        und sie dann falls vorhanden mit aktuellen Bestellungen zu füllen
            */

        private MainWindowVM Main;

        //Property zum wechseln der UserControls in TablesWindow
        private int _SwitchView;
        public int SwitchView
        {
            get { return _SwitchView; }
            set
            {

                _SwitchView = value;
                OnPropertyChanged(nameof(SwitchView));
            }
        }

        public RelayCommand<object> SCurrentOrder { get; set; }
        public RelayCommand<object> SNewOrder { get; set; }

        public TablesWindowVM(MainWindowVM main)
        {
            Main = main;
            SwitchView = 0;
            SCurrentOrder = new RelayCommand<object>(x => SwitchToCurrentOrder());
            SNewOrder = new RelayCommand<object>(x => SwitchToNewOrder());

        }
        public void SwitchToNewOrder()
        {
            SwitchView = 1;
        }

        public void SwitchToCurrentOrder()
        {
            SwitchView = 0;
        }


    }
}
