﻿using De.SandroMaierhofer.POS_Michael.Logic.Ui.Messages;
using De.SandroMaierhofer.POS_Michael.Logic.Ui.Services;
using De.SandroMaierhofer.POS_Michael.Logic.Ui.Wrapper;
using De.SandroMaierhofer.POS_Michael.Services.MediatorServiceBus;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace De.SandroMaierhofer.POS_Michael.Logic.Ui.ViewModel
{
    public class BillWindowVM : ViewModelBase
    {
        #region Properties
        public MainWindowVM Main { get; }
        private TableVM _Table;
        private Printer Printer = new Printer();
        private int Status = 0; //1 = Confirmed, 2 = Payed
        private bool _SyncTable;
        public bool SyncTable
        {
            get { return _SyncTable; }
            set
            {
                _SyncTable = value;
                if (SyncTable)
                {
                    SyncTables(Main.BillTableToCalculate, Table);
                }
                Console.WriteLine("Status changed boolean");
                OnPropertyChanged(nameof(SyncTable));

            }
        }
        private string _TextBoxStatus;
        public string TextBoxStatus { get { return _TextBoxStatus; } set { _TextBoxStatus = value; OnPropertyChanged(nameof(TextBoxStatus)); } }
        public TableVM Table
        {
            get { return _Table; }
            set
            {
                _Table = value;
                OnPropertyChanged(nameof(Table));
            }
        }
        #endregion
        #region RelayCommands
        public RelayCommand<object> CloseControl { get; set; }
        public RelayCommand<object> BillButtonSplit { get; set; }
        public RelayCommand<object> BillButtonPayed { get; set; }
        public RelayCommand<object> BillButtonCompany { get; set; }
        public RelayCommand<object> BillButtonPerson { get; set; }
        public RelayCommand<object> MinusButton { get; set; }
        public RelayCommand<object> PlusButton { get; set; }
        #endregion
        //Constructor
        public BillWindowVM(MainWindowVM m)
        {
            Main = m;
            Console.WriteLine("BillWindowOpen");
            BillButtonSplit = new RelayCommand<object>(x => BillSplit());
            BillButtonCompany = new RelayCommand<object>(x => BillCompany());
            BillButtonPerson = new RelayCommand<object>(x => BillPerson());
            MinusButton = new RelayCommand<object>(x => ChangeCountProduct(1, x));
            PlusButton = new RelayCommand<object>(x => ChangeCountProduct(0, x));
            BillButtonPayed = new RelayCommand<object>(x => BillPayed());
            CloseControl = new RelayCommand<object>(x => CloseWindow());
            SyncTable = Main.BillSplitIsPayed;

        }
        //Function for the CLoseButton (X)
        private void CloseWindow()
        {

            if (Status == 2 || Status == 0)
            {
                switch (Status)
                {
                    case 2:
                        //Table Order Sync
                        SyncTables(Main.CurrentTable, Table);
                        SetSumTable(Main.CurrentTable);
                        if (CheckIfTableIsEmpty(Main.CurrentTable))
                        {
                            MessageBox.Show("Table ist jetzt leer");
                            //Do Stuff to clear Table and Order
                            string query = "UPDATE michael.tables SET status = 4 WHERE `id` = " + Main.CurrentTable.Id;  //Tisch Status 3
                            Main.SQLHelper.EstablishConnection();
                            Main.SQLHelper.SetQuery(query);
                            query = "UPDATE michael.orders SET order_status = 4 WHERE `id` = " + Main.CurrentTable.OrderId;  //Order Status 4
                            Main.SQLHelper.SetQuery(query);

                            Main.SQLHelper.CloseConnection();
                        }
                        else
                        {
                            MessageBox.Show("Table ist nicht leer!**");
                        }
                        ServiceBus.Instance.Send(new CloseBillWindow(Main));
                        break;
                    case 0:
                        ServiceBus.Instance.Send(new CloseBillWindow(Main));
                        break;
                }

            }
            else
            {
                //Abbort
                MessageBoxResult result = MessageBox.Show("Wollen Sie den Vorgang wirklich abbrechen?", "BillWindow", MessageBoxButton.YesNo);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        //Task to do to cancel the order
                        Main.SQLHelper.EstablishConnection();
                        string query = "DELETE FROM michael.bill where `id` =" + Table.RechnungNr;
                        Main.SQLHelper.QueryRequest(query);
                        Main.SQLHelper.CloseConnection();
                        break;
                    case MessageBoxResult.No:

                        break;

                }
            }


        }
        private void BillPayed()
        {
            //If Bill is Payed
            Status = 2;
            TextBoxStatus = "Rechnung drucken oder Fenster schließen";
            ServiceBus.Instance.Send(new OpenPayedWIndow(Main));

        }

        public void SetTable(TableVM t)
        {
            Table = new TableVM(t);
            TextBoxStatus = "Produkte auswählen";
            Status = 0;
        }
        private void ChangeCountProduct(int type, object o)
        {
            ProductVM tempP;
            if (type == 0)
            {
                //Plus
                try
                {
                    tempP = (ProductVM)o;
                    foreach (ProductVM p in Main.CurrentTable.ProductVM)
                    {
                        if (p.Product_name == tempP.Product_name)
                        {
                            Console.WriteLine(p.Count + " =! " + tempP.Count);
                            if (p.Count > tempP.Count)
                            {
                                tempP.Count++;
                            }
                        }
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception" + e);
                }

            }
            else if (type == 1)
            {
                //Minus
                try
                {
                    tempP = (ProductVM)o;
                    foreach (ProductVM p in Main.CurrentTable.ProductVM)
                    {
                        if (p.Product_name == tempP.Product_name)
                        {
                            if (tempP.Count > 0)
                            {
                                tempP.Count--;
                            }
                        }
                    }


                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception" + e);
                }
            }
        }
        private void BillSplit()
        {
            if (Status == 0)
            {
                //This function create a new temp table and 
                ClearTableProductsIfZero(Table);
                SetSumTable(Table);
                Boolean result = CreateBill(Table);
                if (result)
                {
                    TextBoxStatus = "Bitte Bezahlen";
                    Status = 1;
                }
            }

        }
        private void BillPerson()
        {
            Printer = new Printer();
            Printer.SetTable(Table);
            Printer.PrintReceiptForTransaction();
        }
        private void BillCompany()
        {
            Printer = new Printer();
            Printer.SetTable(Table);
            Printer.SetBewirtung();
            Printer.PrintReceiptForTransaction();
        }
        //Setting Sum Properie of Table
        private void SetSumTable(TableVM t)
        {
            t.Sum = 0;
            foreach (ProductVM p in t.ProductVM)
            {
                if (p.Count > 0)
                {
                   
                     t.Sum += p.Price;

                }

            }
        }
        private void SyncTables(TableVM sync, TableVM compare)
        {
            foreach (ProductVM pts in sync.ProductVM)
            {
                foreach (ProductVM ptc in compare.ProductVM)
                {
                    if (pts.ID == ptc.ID)
                    {
                        pts.Count -= ptc.Count;
                    }
                }
            }
            ClearTableProductsIfZero(sync);
        }
        private void ClearTableProductsIfZero(TableVM t)
        {
            TableVM temp = new TableVM(t);
            foreach (ProductVM p in temp.ProductVM)
            {
                if (p.Count == 0)
                {
                    foreach (ProductVM pp in t.ProductVM)
                    {
                        if (pp.ID == p.ID)
                        {
                            t.ProductVM.Remove(pp);
                            break;
                        }
                    }

                }

            }
        }
        private bool CreateBill(TableVM t)
        {

            //Rechnung erstellen
            //establish connection
            Main.SQLHelper.EstablishConnection();
            //create date
            string d = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            //Sum to string and replace , with .
            string pro = t.Sum.ToString();
            pro = pro.Replace(@",", ".");
            //Create bill in db
            string query = " INSERT INTO `michael`.`bill` (`order_id`, `euro`, `order_date`, `status_id`) VALUES ('" + t.OrderId + "', '" + pro + "', '" + d + "', '1');";
            Main.SQLHelper.SetQuery(query);

            //RechnungsID speichern 
            query = "Select `id` from michael.bill where `order_id` = " + Main.CurrentTable.OrderId + " and `order_date` =  \"" + d + " \" ;";
            DataSet ds = Main.SQLHelper.QueryRequest(query);
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                Console.WriteLine(dr[0]);
                Main.CurrentTable.RechnungNr = Convert.ToInt16(dr[0]);
            }

            //Rechnung_Produkte erstellen in DB
            foreach (ProductVM pvm in Main.CurrentProducts)
            {
                string tempPrice = pvm.Price.ToString();
                tempPrice = tempPrice.Replace(",", ".");
                query = " INSERT INTO `michael`.`bill_items` (`bill_id`, `product_name`, `price`, `tax`,`amount`) VALUES ('" + Main.CurrentTable.RechnungNr + "', '" + pvm.Product_name + "', '" + tempPrice + "', '" + pvm.Tax + "', '" + pvm.Count + "');";
                Main.SQLHelper.SetQuery(query);
            }

            Main.SQLHelper.CloseConnection();
            return true;
        }
        private bool CheckIfTableIsEmpty(TableVM t)
        {
            if (t.ProductVM.Count == 0)
            {
                //Table Status ändern                
                Main.SQLHelper.EstablishConnection();
                string query = "UPDATE michael.tables SET status = 3 WHERE `id` = " + t.Id;  //Tisch Status 3
                Main.SQLHelper.SetQuery(query);
                Main.SQLHelper.CloseConnection();
                return true;

            }
            else
            {
                return false;
            }

        }

    }
}
