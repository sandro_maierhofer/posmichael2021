﻿using De.SandroMaierhofer.POS_Michael.Logic.Ui.Services;
using De.SandroMaierhofer.POS_Michael.Logic.Ui.Wrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace De.SandroMaierhofer.POS_Michael.Logic.Ui.ViewModel
{
    public class ProductsUserControlVM : ViewModelBase
    {
        public MainWindowVM Main { get; set; }

        public RelayCommand<object> SendCurrentItem { get; set; }

        public ProductsUserControlVM(MainWindowVM main)
        {
            Main = main;
            SendCurrentItem = new RelayCommand<object>(x => AddProductToCurrentTable(x));
        }

        public void AddProductToCurrentTable(object o)
        {
            ProductVM pcVM = (ProductVM)o;
            //Aktueller Tisch darf nicht null sein!
            if (Main.CurrentTable != null)
            {
                bool present = false;
                foreach (ProductVM p in Main.CurrentProducts)
                {
                    if (p.Product_name == pcVM.Product_name)
                    {
                        p.Price += pcVM.Price;
                        p.Count++;
                        Main.Sum += pcVM.Price;
                        present = true;
                        Main.ProductsAreChanged = true;
                        break;
                    }

                }
                if (!present)
                {
                    Main.CurrentProducts.Add(new ProductVM(pcVM));
                    Main.Sum += pcVM.Price;
                    Main.ProductsAreChanged = true;
                }

            }
            else
            {
                MessageBox.Show("Bitte wählen Sie einen Tisch/Person aus");
            }


        }
    }
}
