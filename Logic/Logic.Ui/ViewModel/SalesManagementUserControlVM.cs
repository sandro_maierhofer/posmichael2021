﻿using De.SandroMaierhofer.POS_Michael.Logic.Ui.Services;
using De.SandroMaierhofer.POS_Michael.Logic.Ui.Wrapper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Forms;

namespace De.SandroMaierhofer.POS_Michael.Logic.Ui.ViewModel
{
    public class SalesManagementUserControlVM : ViewModelBase
    {
        #region Properties
        public MainWindowVM Main { get; set; }
        private string _Sum;
        public string Sum
        {
            get { return _Sum; }
            set
            {
                _Sum = value;
                OnPropertyChanged(nameof(Sum));
            }
        }
        private DateTime _DTPStart;
        public DateTime DTPStart
        {
            get { return _DTPStart; }
            set
            {
                _DTPStart = value;
                OnPropertyChanged(nameof(DTPStart));
            }
        }
        private DateTime _DTPEnd;
        public DateTime DTPEnd {
            get { return _DTPEnd; }
            set
            {
                _DTPEnd = value;
                OnPropertyChanged(nameof(DTPEnd));
            }
        }
        private int _TimeStart;
        public int TimeStart
        {
            get { return _TimeStart; }
            set
            {
                _TimeStart = value;
                OnPropertyChanged(nameof(TimeStart));
            }
        }

        private int _TimeEnd;
        public int TimeEnd
        {
            get { return _TimeEnd; }
            set
            {
                _TimeEnd = value;
                OnPropertyChanged(nameof(TimeEnd));
            }
        }

        private List<UserVM> _User;
        public List<UserVM> User
        {
            get { return _User; }
            set
            {
                _User = value;
                OnPropertyChanged(nameof(User));
            }
        }
        private UserVM _SelectedUser;
        public UserVM SelectedUser
        {
            get { return _SelectedUser; }
            set
            {
                _SelectedUser = value;
                OnPropertyChanged(nameof(SelectedUser));
            }
        }

        private ObservableCollection<ProductVM> _SalesProducts;
        public ObservableCollection<ProductVM> SalesProducts
        {
            get { return _SalesProducts; }
            set
            {
                _SalesProducts = value;
                OnPropertyChanged(nameof(SalesProducts));
            }
        }
        #endregion
        #region RelayCommands
        public RelayCommand<object> CreateDocument { get; set; }

        #endregion
        public SalesManagementUserControlVM(MainWindowVM main)
        {
            Main = main;
            CreateDocument = new RelayCommand<object>(x => SearchDBForRange());
            DTPEnd = DateTime.Now;
            DTPStart = DateTime.Now;
            TimeEnd = 18;
            TimeStart = 8;
            SearchForUser();
            SalesProducts = new ObservableCollection<ProductVM>();
        }

        private void CreateSalesDocument()
        {
            PDFCreator pdf = new PDFCreator();
            var list = new List<ProductVM>();

            foreach(ProductVM p in SalesProducts)
            {
                list.Add(p);
            }

            pdf.CreatePDFListSalesManagement(list, DTPStart, DTPEnd);
        }

        private void SearchDBForRange()
        {
            SalesProducts = new ObservableCollection<ProductVM>();
            Main.SQLHelper.EstablishConnection();
            DateTime DTPEndTemp = DTPEnd.AddHours(TimeEnd);
            DateTime DTPStartTemp = DTPStart.AddHours(TimeStart);
            string query ="";
            if (SelectedUser != null)
            {
                try
                {
                    query = "SELECT id FROM michael.orders WHERE employee_id = '" + SelectedUser.ID + "' && order_status != '1' && order_status != '2' && order_date > '" + DTPStartTemp.ToString("yyyy-MM-dd HH:mm:ss") + "' && order_date <= '" + DTPEndTemp.ToString("yyyy-MM-dd HH:mm:ss") + "' ";
                }catch(Exception e)
                {
                    MessageBox.Show(e.Message);
                }
                
            }
            else
            {
                try
                {
                    query = "SELECT id FROM michael.orders WHERE order_date > '" + DTPStartTemp.ToString("yyyy-MM-dd HH:mm:ss") + "'  && order_date <= '" + DTPEndTemp.ToString("yyyy-MM-dd HH:mm:ss") + "' && order_status != '1' && order_status != '2' ";
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            }
            
            DataSet ds = Main.SQLHelper.QueryRequest(query);
            //List for queryrequest -> OrderID´s
            List<int> orderIDs = new List<int>();

            try
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    orderIDs.Add(Convert.ToInt16(dr[0]));
                }

                //Create Query for BILL ID´s
                query = "SELECT id FROM michael.bill WHERE order_id IN ('";
                foreach (int a in orderIDs)
                {
                    query += a.ToString() + "','";
                }
                query += "');";

                //List<ProductOrderVM> po = new List<ProductOrderVM>(); //All Products Order ID + Count
                ds = Main.SQLHelper.QueryRequest(query);
                List<int> billIDs = new List<int>();
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    billIDs.Add(Convert.ToInt16(dr[0]));
                }
                if (DTPStart >= new DateTime(2021, 11, 3, 0, 0, 0))
                {
                    Console.WriteLine("Neue Suche!!!!!");
                    //Get all products from bill_items
                    query = "SELECT * FROM michael.bill_items WHERE bill_id IN ('";
                    foreach (int a in billIDs)
                    {
                        query += a.ToString() + "','";
                    }
                    query += "');";
                    ds = Main.SQLHelper.QueryRequest(query);
                    List<ProductVM> Products = new List<ProductVM>(); //List of Products ´with just name, price and count

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        Products.Add(new ProductVM
                        {
                            Product_name = dr[2].ToString(),
                            Price = Convert.ToDouble(dr[3]),
                            Count = Convert.ToInt16(dr[5])
                        });
                    }
                    double TempSum = 0;
                    foreach (ProductVM pvm in Products) //Alle Produkte
                    {

                        bool ispresent = false;
                        foreach (ProductVM sapro in SalesProducts)//Produkte zum Anzeigen
                        {
                            double listProductprice = pvm.Price / pvm.Count;
                            double Productprice = sapro.Price / sapro.Count;
                            listProductprice = Math.Round(listProductprice, MidpointRounding.ToEven);
                            Productprice = Math.Round(Productprice, MidpointRounding.ToEven);

                            if (sapro.Product_name == pvm.Product_name && Productprice == listProductprice)
                            {
                                sapro.Count += pvm.Count;
                                sapro.Price += pvm.Price;
                                ispresent = true;
                                TempSum += pvm.Price;
                                break;

                            }
                        }
                        if (!ispresent)
                        {
                            SalesProducts.Add(new ProductVM(pvm));
                            TempSum += pvm.Price;
                        }



                    }
                    Sum = string.Format("€{0:N2} Euro", TempSum);
                    Main.SQLHelper.CloseConnection();
                    Console.WriteLine(SalesProducts.Count);
                    CreateSalesDocument();
                }
                else
                {
                    Console.WriteLine("ALTE Suche!!!!!");
                    //Now Search Products
                    query = "SELECT * FROM michael.products_orders WHERE orders_id IN ('";
                    foreach (int a in orderIDs)
                    {
                        query += a.ToString() + "','";
                    }
                    query += "');";

                    List<ProductOrderVM> po = new List<ProductOrderVM>(); //All Products Order ID + Count
                    ds = Main.SQLHelper.QueryRequest(query);
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        po.Add(new ProductOrderVM
                        {
                            ID = Convert.ToInt16(dr[0]),
                            Count = (int)dr[2]
                        });

                    }

                    //Get all products
                    ds = Main.SQLHelper.QueryRequest("SELECT * FROM michael.products");
                    List<ProductVM> Products = new List<ProductVM>(); //List of Products

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        Products.Add(new ProductVM
                        {
                            ID = Convert.ToInt16(dr[0]),
                            Product_code = dr[1].ToString(),
                            Product_name = dr[2].ToString(),
                            Description = dr[3].ToString(),
                            Price = Convert.ToDouble(dr[4]),
                            Image = @dr[6].ToString(),
                            Count = 1
                        });
                        string a = Products[Products.Count - 1].Image;
                        a = a.Replace(@"\\", @"\");
                        Products[Products.Count - 1].Image = a;
                    }
                    double TempSum = 0;
                    foreach (ProductOrderVM temp in po)//Order Products + Count
                    {
                        foreach (ProductVM pvm in Products) //Alle Produkte
                        {
                            if (temp.ID == pvm.ID)//Product fount in list
                            {
                                bool ispresent = false;
                                foreach (ProductVM sapro in SalesProducts)//Produkte zum Anzeigen
                                {
                                    Console.WriteLine("Test in sapro");
                                    if (sapro.ID == temp.ID)
                                    {
                                        sapro.Count += temp.Count;
                                        ispresent = true;
                                        TempSum += pvm.Price * temp.Count;
                                        Console.WriteLine("Bestehendes Produkt " + pvm.Product_name + " Anzahl: " + temp.Count);
                                        break;

                                    }

                                }
                                if (!ispresent)
                                {
                                    pvm.Count = temp.Count;
                                    SalesProducts.Add(new ProductVM(pvm));
                                    TempSum += pvm.Price * pvm.Count;
                                    Console.WriteLine("Neues Produkt " + pvm.Product_name + " Anzahl: " + temp.Count);
                                }
                                break;
                            }
                        }
                    }
                    Sum = string.Format("€{0:N2} Euro", TempSum);
                    Main.SQLHelper.CloseConnection();
                    Console.WriteLine(SalesProducts.Count);
                }
                

            }
            catch(Exception e)
            {
                MessageBox.Show("Keine Daten für diesen Benutzer!"+e);
            }
            
        }

        private void SearchForUser()
        {
            Main.SQLHelper.EstablishConnection();
            string query = "SELECT * FROM michael.user;";
            DataSet ds = Main.SQLHelper.QueryRequest(query);
            User = new List<UserVM>();
            foreach (DataRow dr in ds.Tables[0].Rows)
            {

                User.Add(new UserVM
                {
                    ID = (int)dr[0],
                    Username = dr[1].ToString()
                });
                    
                
            }
            Main.SQLHelper.CloseConnection();
        }
    }
}
