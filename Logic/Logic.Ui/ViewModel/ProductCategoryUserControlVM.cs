﻿using De.SandroMaierhofer.POS_Michael.Logic.Ui.Services;
using De.SandroMaierhofer.POS_Michael.Logic.Ui.Wrapper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De.SandroMaierhofer.POS_Michael.Logic.Ui.ViewModel
{
    public class ProductCategoryUserControlVM
    {
        public ObservableCollection<ProductCategoryVM> Category { get; set; }

        public RelayCommand<object> LoadCategoryCommand { get; set; }

        public RelayCommand<object> CategoryEventButton { get; set; }
        public MainWindowVM Main { get; }
        public ProductCategoryUserControlVM(MainWindowVM vM)
        {
            Main = vM;
            Category = new ObservableCollection<ProductCategoryVM>();
            LoadCategoryCommand = new RelayCommand<object>(x => LoadCategories());
            LoadCategories();
        }

        public void LoadCategories()
        {
            Main.SQLHelper.EstablishConnection();
            DataSet ds = Main.SQLHelper.QueryRequest("SELECT * FROM michael.product_category ORDER BY michael.product_category.order_nr asc");
            Category = new ObservableCollection<ProductCategoryVM>();
            CategoryEventButton = new RelayCommand<object>(x => CategoryButtonEvent(x));

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if ((int)dr[0] != 99)
                {
                    Category.Add(new ProductCategoryVM
                    {
                        ID = (int)dr[0],
                        Name = dr[1].ToString(),


                    });
                }

            }

            Main.SQLHelper.CloseConnection();
        }

        public void CategoryButtonEvent(object o)
        {

            ProductCategoryVM pc = (ProductCategoryVM)o;
            Main.ProductSearch = new ObservableCollection<ProductVM>();
            Main.BindCategory = false;

            //Suche in DB
            Main.SQLHelper.EstablishConnection();
            DataSet ds;
            ds = Main.SQLHelper.QueryRequest("SELECT * FROM michael.products p  INNER JOIN michael.product_category pc 	ON p.category = pc.id WHERE pc.name = '" + pc.Name + "'");


            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                Main.ProductSearch.Add(new ProductVM
                {
                    ID = Convert.ToInt16(dr[0].ToString()),
                    Product_code = dr[1].ToString(),
                    Product_name = dr[2].ToString(),
                    Description = dr[3].ToString(),
                    Price = Convert.ToDouble(dr[4]),
                    Image = @dr[6].ToString(),
                    Tax = Convert.ToInt16(dr[7]),
                    Count = 1


                });

            }
            Main.SQLHelper.CloseConnection();
        }
    }
}
