﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace De.SandroMaierhofer.POS_Michael.Data.SQLHelper
{
    public class SQLHelper
    {

        private MySqlConnection connection;
        private MySqlCommand cmd = null;
        private MySqlDataAdapter adapter;
        private DataSet ds;

        public void CloseConnection()
        {
            connection.Close();
        }

        public DataSet QueryRequest(string SQLCommand)
        {
            try
            {
                cmd = new MySqlCommand(SQLCommand, connection);
                adapter = new MySqlDataAdapter(cmd);
                ds = new DataSet();
                adapter.Fill(ds, "michael.products");

                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

                return ds;
            }

        }
        public void SetQuery(string sqlcommand)
        {
            cmd = new MySqlCommand(sqlcommand, connection);
            cmd.ExecuteNonQuery();

        }
        public void EstablishConnection()
        {
            MySqlConnectionStringBuilder builder = new MySqlConnectionStringBuilder();
            try
            {
                //For Server - Client
                //builder.Server = "192.168.1.201";
                //builder.UserID = "Sandro";
                //builder.Password = "yk7m!Mb35mkbwshBGO989z";

                //For dev
                builder.Server = "localhost";
                builder.UserID = "root";
                builder.Password = "AS43esgeRz34DSA";

                builder.Database = "michael";
                builder.SslMode = MySqlSslMode.Required;
                connection = new MySqlConnection(builder.ToString());
                connection.Open();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Connection Failed!... Message:  " + ex.Message);
            }
            
        }


    }
}
