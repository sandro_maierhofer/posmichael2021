﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De.SandroMaierhofer.POS_Michael.Business.Model.BusinessObjects
{
    public class Product
    {
        public string product_code { get; set; }
        public string product_name { get; set; }
        public string description { get; set; }
        public double price { get; set; }
        public string img { get; set; }
        public int ID { get; set; }
        public int count { get; set; }
        public int category { get; set; }
        public int tax { get; set; }
    }
}
