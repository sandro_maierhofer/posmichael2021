﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De.SandroMaierhofer.POS_Michael.Business.Model.BusinessObjects
{
    public class Table
    {
        public string Name { get; set; }
        public ObservableCollection<Product> Products { get; set; } = new ObservableCollection<Product>();
        public int Status { get; set; }
        public double Sum { get; set; }
        public int EmployeeID { get; set; }
        public string ShortName { get; set; }
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int RechnungNr { get; set; }
    }
}
