﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De.SandroMaierhofer.POS_Michael.Business.Model.BusinessObjects
{
    public class User
    {
        public int ID;
        public string Name;
        public string Username;
        public string Password;
        public string PicPath;
        public string Role;
    }
}
