﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De.SandroMaierhofer.POS_Michael.Business.Model.BusinessObjects
{
    public class Bill
    {
        public int ID;
        public int Order_ID;
        public double Euro;
        public DateTime Order_Date;
        public int TransactionTyp;
        public int Status;
    }
}
