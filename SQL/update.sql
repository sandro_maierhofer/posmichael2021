CREATE TABLE IF NOT EXISTS `michael`.`bill_items` (
  `id` INT(16) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `bill_id` INT(11) unsigned zerofill NOT NULL,
  `product_name` VARCHAR(25) NOT NULL,
  `price` FLOAT(12) NOT NULL,
  `tax` FLOAT(12) NOT NULL,
  `amount` INT(2) NOT NULL,
  `created` INT(2) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `id` (`id` ASC),
  CONSTRAINT `bill_id`
    FOREIGN KEY (`bill_id`)
    REFERENCES `michael`.`bill` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

ALTER TABLE products add `tax` int(2) generated always as (19)