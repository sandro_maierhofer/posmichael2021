﻿SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `michael` ;
CREATE SCHEMA IF NOT EXISTS `michael` DEFAULT CHARACTER SET latin1 ;
USE `michael` ;

-- -----------------------------------------------------
-- Table `michael`.`employees`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `michael`.`employees` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `last_name` VARCHAR(50) NULL DEFAULT NULL,
  `first_name` VARCHAR(50) NULL DEFAULT NULL,
  `email_address` VARCHAR(50) NULL DEFAULT NULL,
  `job_title` VARCHAR(50) NULL DEFAULT NULL,
  `home_phone` VARCHAR(25) NULL DEFAULT NULL,
  `mobile_phone` VARCHAR(25) NULL DEFAULT NULL,
  `address` LONGTEXT NULL DEFAULT NULL,
  `city` VARCHAR(50) NULL DEFAULT NULL,
  `state_province` VARCHAR(50) NULL DEFAULT NULL,
  `zip_postal_code` VARCHAR(15) NULL DEFAULT NULL,
  `country_region` VARCHAR(50) NULL DEFAULT NULL,
  `notes` LONGTEXT NULL DEFAULT NULL,
  `attachments` LONGBLOB NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `city` (`city` ASC),
  INDEX `first_name` (`first_name` ASC),
  INDEX `last_name` (`last_name` ASC),
  INDEX `zip_postal_code` (`zip_postal_code` ASC),
  INDEX `state_province` (`state_province` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `michael`.`transaction_types`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `michael`.`transaction_types` (
  `id` TINYINT(2) NOT NULL auto_increment,
  `type_name` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `michael`.`bill_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `michael`.`bill_status` (
  `id` TINYINT(2) NOT NULL AUTO_INCREMENT,
  `status_name` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `michael`.`bill`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `michael`.`bill` (
  `id` INT(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `order_id` INT(11) unsigned zerofill NOT NULL,
  `euro` FLOAT(12) NOT NULL,
  `order_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_id` TINYINT(2) NOT NULL DEFAULT 0, 
  `bill_transaction_types_id` TINYINT(2) NOT NULL DEFAULT 3,
  PRIMARY KEY (`id`),
  INDEX `id` (`id` ASC),
  CONSTRAINT `bill_transaction_types_id`
    FOREIGN KEY (`bill_transaction_types_id`)
    REFERENCES `michael`.`transaction_types` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `status_id`
    FOREIGN KEY (`status_id`)
    REFERENCES `michael`.`bill_status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `order_id`
    FOREIGN KEY (`order_id`)
    REFERENCES `michael`.`orders` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `michael`.`orders_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `michael`.`orders_status` (
  `id` INT(2) NOT NULL AUTO_INCREMENT,
  `status_name` VARCHAR(50) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `id` (`id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `michael`.`orders`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `michael`.`orders` (
  `id` INT(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `employee_id` INT(11) NULL DEFAULT NULL,
  `table` INT(11) NOT NULL,
  `order_date` DATETIME NULL DEFAULT NULL,
  `order_status` INT(2) NOT NULL DEFAULT 0,
  `notes` LONGTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `employee_id` (`employee_id` ASC),
  INDEX `id` (`id` ASC),
 CONSTRAINT `table`
    FOREIGN KEY (`table`)
    REFERENCES `michael`.`tables` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
 CONSTRAINT `order_status`
    FOREIGN KEY (`order_status`)
    REFERENCES `michael`.`orders_status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;



-- -----------------------------------------------------
-- Table `michael`.`product_category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `michael`.`product_category` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(25) NULL DEFAULT NULL,
  `order_nr` INT(3) NOT NULL DEFAULT 200,
  PRIMARY KEY (`id`),
  INDEX `name` (`name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `michael`.`products`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `michael`.`products` (
  `id` INT(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `product_code` VARCHAR(25) NULL DEFAULT NULL,
  `product_name` VARCHAR(80) NULL DEFAULT NULL,
  `description` LONGTEXT NULL DEFAULT NULL,
  `list_price` DECIMAL(10,4) NOT NULL DEFAULT '0.0000',
  `category` INT(11) NOT NULL DEFAULT 0,
  `pic_path` VARCHAR(255) NOT NULL DEFAULT 'C:\\Windows\\SystemApps\\Microsoft.Windows.Cortana_cw5n1h2txyewy\\Assets\\Icons\\custom-Cortana\\SmallTile.scale-200.png',
  PRIMARY KEY (`id`),
  INDEX `product_code` (`product_code` ASC),
CONSTRAINT `category`
    FOREIGN KEY (`category`)
    REFERENCES `michael`.`product_category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `michael`.`products_order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `michael`.`products_orders` (
  `product_id` int(11) unsigned zerofill not null,
  `orders_id` int(11) unsigned zerofill not null,
  `number_product` INT(2) NULL DEFAULT 1,
  `note` LONGTEXT NULL DEFAULT NULL,
  primary key(`product_id`,`orders_id`),
CONSTRAINT `product_id`
    FOREIGN KEY (`product_id`)
    REFERENCES `michael`.`products` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
CONSTRAINT `orders_id`
    FOREIGN KEY (`orders_id`)
    REFERENCES `michael`.`orders` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Table `michael`.`table_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `michael`.`table_status` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `status_name` VARCHAR(50) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `id` (`id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `michael`.`tables`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `michael`.`tables` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  `shortname` VARCHAR(5) NOT NULL,
  `seats` int(2) NULL DEFAULT 1,
  `status` INT(2) NOT NULL DEFAULT 4,
  PRIMARY KEY (`id`),
  INDEX `id` (`id` ASC),
  CONSTRAINT `status`
    FOREIGN KEY (`status`)
    REFERENCES `michael`.`table_status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `michael`.`user`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `michael`.`user` (
  `id` INT(11) NOT NULL auto_increment,
  `username` VARCHAR(50) NOT NULL,
  `password` VARCHAR(50) NOT NULL,
  `role` VARCHAR(50) NULL DEFAULT "service",
  `employee_id` INT(11) NOT NULL,
  `status` INT(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  INDEX `id` (`id` ASC),
  CONSTRAINT `employee_id`
    FOREIGN KEY (`employee_id`)
    REFERENCES `michael`.`employees` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- -----------------DATA--------------------------------
-- -----------------------------------------------------

-- --------------------------------Getränke-----------------------------
INSERT INTO `michael`.`product_category` (`name`, `id`) VALUES ('Bier',1);
INSERT INTO `michael`.`product_category` (`name`, `id`) VALUES ('Softdrinks',2);
INSERT INTO `michael`.`product_category` (`name`, `id`) VALUES ('Wein',3);
INSERT INTO `michael`.`product_category` (`name`, `id`) VALUES ('Kaffee',4);
INSERT INTO `michael`.`product_category` (`name`, `id`) VALUES ('Spiritoosen',5);
INSERT INTO `michael`.`product_category` (`name`, `id`) VALUES ('Longdrinks',6);
-- --------------------------------Speisen-----------------------------
INSERT INTO `michael`.`product_category` (`name`, `id`) VALUES ('Mittagstisch',10);
INSERT INTO `michael`.`product_category` (`name`, `id`) VALUES ('Suppen',11);
INSERT INTO `michael`.`product_category` (`name`, `id`) VALUES ('kleine Gerichte',12);
-- INSERT INTO `michael`.`product_category` (`name`, `id`) VALUES ('Aus der Pfanne',13);
INSERT INTO `michael`.`product_category` (`name`, `id`) VALUES ('Nudeln',14);
INSERT INTO `michael`.`product_category` (`name`, `id`) VALUES ('Fisch',15);
INSERT INTO `michael`.`product_category` (`name`, `id`) VALUES ('Burger',16);
INSERT INTO `michael`.`product_category` (`name`, `id`) VALUES ('Klassiker',17);
-- --------------------------------Nicht Sortiert ----------------------------
INSERT INTO `michael`.`product_category` (`name`, `id`) VALUES ('Salate',20);
-- INSERT INTO `michael`.`product_category` (`name`, `id`) VALUES ('Vorspeisen',21);
INSERT INTO `michael`.`product_category` (`name`, `id`) VALUES ('Schnitzel',22);
INSERT INTO `michael`.`product_category` (`name`, `id`) VALUES ('Steak',23);
INSERT INTO `michael`.`product_category` (`name`, `id`) VALUES ('Desserts',24);
INSERT INTO `michael`.`product_category` (`name`, `id`) VALUES ('Weihnachten',25);
INSERT INTO `michael`.`product_category` (`name`, `id`) VALUES ('Kinderkarte',26);
INSERT INTO `michael`.`product_category` (`name`, `id`) VALUES ('Menkes Knolle',27);
INSERT INTO `michael`.`product_category` (`name`, `id`) VALUES ('Beilagen',28);

-- --------------------------------Sonstige ----------------------------
INSERT INTO `michael`.`product_category` (`name`,`id`) VALUES ('Rabatt', '99');
INSERT INTO `michael`.`product_category` (`name`,`id`) VALUES ('Sonstige', '999');

INSERT INTO `michael`.`table_status` (`status_name`) VALUES ('new');
INSERT INTO `michael`.`table_status` (`status_name`) VALUES ('progress');
INSERT INTO `michael`.`table_status` (`status_name`) VALUES ('done');
INSERT INTO `michael`.`table_status` (`status_name`) VALUES ('empty');

INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Tisch 1', '8','T01');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Tisch 2', '8','T02');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Tisch 3', '8','T03');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Tisch 4', '8','T04');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Tisch 5', '8','T05');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Tisch 6', '8','T06');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Tisch 7', '8','T07');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Tisch 8', '8','T08');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Tisch 9', '8','T09');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Tisch 10', '8','T10');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Tisch 11', '8','T11');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Tisch 12', '8','T12');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Tisch 13', '8','T13');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Tisch 14', '8','T14');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Tisch 15', '8','T15');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Tisch 16', '8','T16');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Tisch 17', '8','T17');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Tisch 18', '8','T18');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Tisch 19', '8','T19');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Tisch 20', '8','T20');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Tisch 21', '8','T21');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Tisch 22', '8','T22');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Tisch 23', '8','T23');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Tisch 24', '8','T24');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Tisch 25', '8','T25');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Tisch 26', '8','T26');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Tisch 27', '8','T27');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('AH', '8','AH');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Bar 1', '1','B01');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Bar 2', '1','B02');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Bar 3', '1','B03');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Bar 4', '1','B04');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Bar 5', '1','B05');
INSERT INTO `michael`.`tables` (`name`, `seats`,`shortname`) VALUES ('Bar 6', '1','B06');

-- -----------------------------------------SpeiseKarte--------------------------------------------
-- ----------------------------------------------Biere 1 (B)---------------------------------------------------
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('B01', 'Lüneburger Pils 0/3', 'Lüneburger Pils 0/3 L', '2.70', '1');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('B02', 'Lüneburger Pils 0/5', 'Lüneburger Pils 0/5 L', '4.20', '1');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('B03', 'Duckstein 0/3', 'Flasche Duckstein 0,33l', '2.90', '1');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('B04', 'Duckstein 0/5', 'Flasche Duckstein 0,5l', '4.40', '1');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('B05', 'Alsterwasser 0/3', 'Alsterwasser/ Biermit mit Wasser/ Bier mit Cola', '2.80', '1');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('B05', 'Diesel 0/3', 'Alsterwasser/ Biermit mit Wasser/ Bier mit Cola', '2.80', '1');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('B05', 'BMW 0/3', 'Alsterwasser/ Biermit mit Wasser/ Bier mit Cola', '2.80', '1');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('B06', 'Alsterwasser 0/5', 'Alsterwasser/ Biermit mit Wasser/ Bier mit Cola', '4.40', '1');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('B06', 'Diesel 0/5', 'Alsterwasser/ Biermit mit Wasser/ Bier mit Cola', '4.40', '1');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('B06', 'BMW 0/5', 'Alsterwasser/ Biermit mit Wasser/ Bier mit Cola', '4.40', '1');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('B07', 'Erdinger hell 0/5', 'Erdinger Flasche 0,5', '4.20', '1');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('B07', 'Erdinger dunkel 0/5', 'Erdinger Flasche 0,5', '4.20', '1');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('B07', 'Erdinger alk. frei 0/5', 'Erdinger Flasche 0,5', '4.20', '1');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('B08', 'Schöfferhöfer Grapefruit 0,33', 'Mischbier oder Fassbrause 0/3 L', '2.70', '1');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('B09', 'Krombacher Fassbrause Mango 0/3', 'Krombacher Fassbrause Mango 0/3 L', '2.70', '1');

-- --------------------------------------------------Softdrink 2(AF)---------------------------------------------
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF01', 'Mineralwasser 0/2', 'Wasser mit Sprudel', '2.30', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF02', 'Mineralwasser 0/75', 'Wasser mit Sprudel', '7.50', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF03', 'Fanta 0/3', 'Softdrinks 0,3', '2.80', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF03', 'Cola 0/3', 'Softdrinks 0,3', '2.80', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF03', 'Sprite 0/3', 'Softdrinks 0,3', '2.80', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF04', 'Fanta 0/5', 'Softdrinks 0,5', '4.40', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF04', 'Cola 0/5', 'Softdrinks 0,5', '4.40', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF04', 'Sprite 0/5', 'Softdrinks 0,5', '4.40', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF05', 'Apfelschorle 0/3', 'Apfel ', '2.80', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF05', 'Orangeschorle 0/3', 'Orange', '2.80', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF05', 'Traubeschorle 0/3', 'Traube', '2.80', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF05', 'Kirscheschorle 0/3', 'Kirsche', '2.80', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF05', 'Bananeschorle 0/3', 'Banane', '2.80', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF05', 'Mangoschorle 0/3', 'Mango', '2.80', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF05', 'Ananasschorle 0/3', 'Ananas', '2.80', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF05', 'Maracujaschorle 0/3', 'Maracuja', '2.80', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF05', 'Cranberryschorle 0/3', 'Cranberry', '2.80', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF05', 'Tomateschorle 0/3', 'Tomate', '2.80', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF06', 'Apfelschorle 0/5', 'Apfel', '4.40', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF06', 'Orangeschorle 0/5', 'Orange', '4.40', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF06', 'Traubeschorle 0/5', 'Traube', '4.40', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF06', 'Kirscheschorle 0/5', 'Kirsche', '4.40', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF06', 'Bananeschorle 0/5', 'Banane', '4.40', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF06', 'Mangoschorle 0/5', 'Mango', '4.40', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF06', 'Ananasschorle 0/5', 'Ananas', '4.40', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF06', 'Maracujaschorle 0/5', 'Maracuja', '4.40', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF06', 'Cranberryschorle 0/5', 'Cranberry', '4.40', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF06', 'Tomatenschorle 0/5', 'Tomate', '4.40', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF07', 'Saft 0/3', 'Softdrinks', '2.60', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF08', 'Saft 0/5', 'Softdrinks', '3.50', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF09', 'Schweppes Tonic 0/5', 'Softdrinks', '2.40', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF09', 'Schweppes Ginger Ale 0/5', 'Softdrinks', '2.40', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF09', 'Schweppes Bitter Lemon 0/5', 'Softdrinks', '2.40', '2');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('AF10', 'Redbull 0/25', 'Softdrinks', '3.50', '2');

-- ------------------------------------------------------Wein 3(W)-------------------------------------------------
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('W01', 'Wein 0,25', 'Rotwein', '4.20', '3');

-- ------------------------------------------------------Kaffee(Heiße Getränke) 4(K)-----------------------------------------------
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('K01', 'Kaffee Crema', '', '2.20', '4');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('K02', 'Espresso', '', '2.20', '4');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('K03', 'Cappuccino', '', '2.70', '4');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('K04', 'Latte Macchiato', '', '2.80', '4');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('K05', 'Milchkaffee', '', '3.20', '4');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('K06', 'Kännchen Kaffee', '', '4.20', '4');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('K07', 'Becher Kaffee', '', '2.80', '4');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('K08', 'Doppelter Espresso', '', '3.80', '4');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('K09', 'Irish Coffee', '', '4.50', '4');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('K10', 'Kaffee mit Whisky & Sahne', '', '4.00', '4');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('K11', 'Heiße Schokolade m.M.', '', '2.50', '4');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('K12', 'Heiße Schokolade m.M&S.', '', '2.80', '4');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('K13', 'Heiße Schokolade m. Schuss', '', '4.50', '4');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('K14', 'Tee', '', '2.20', '4');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('K15', 'Kännchen Tee', '', '4.20', '4');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('K16', 'Rum Grog', '', '4.50', '4');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('K17', 'Glühwein', '', '4.50', '4');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('K18', 'Eisbrecher- Weingrog', '', '4.50', '4');

-- ------------------------------------------------------Spiritoosen 5(SO)-------------------------------------------
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('S01', 'Korn', '', '2.50', '5');

-- ------------------------------------------------------Longdrinks 6(LD)--------------------------------------------
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('L02', 'Tomatensuppe', 'Ueberraschung', '4.90', '6');

-- ------------------------------------------------------Mittagstisch 10(M)-----------------------------------------
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('F02', 'Kürbissuppe', 'Ueberraschung', '5.90', '10');

-- -----------------------------------------------------Suppen 11(SU)------------------------------------------------
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('SU01', 'Tagessuppe', '', '2.60', '11');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('SU02', 'Tomatensuppe', 'Tomatensuppe mit Sahne', '3.80', '11');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('SU03', 'Wiener Kartoffelsuppe', '', '4.20', '11');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('SU04', 'Gebundene Zwiebelsuppe', '', '4.50', '11');

-- --------------------------------------------------------kleine Gerichte 12(KG) -----------------------------------
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('KG01', 'Brot Käse & Salatgarnitur', '', '6.90', '12');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('KG02', 'Brot Schinken, Ei, Gurke & Salatgarnitur', '', '7.50', '12');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('KG03', 'Toast Hawai mit Salatgarnitur', '', '8.50', '12');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('KG04', 'Gebackener Camenbert', '', '8.00', '12');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('KG05', 'Frühlingsrolle', '', '5.50', '12');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('KG06', 'Matjes', '', '6.50', '12');

-- -------------------------------------------------------Nudeln 14(NU)-----------------------------------------------------------------------------
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('F02', 'Kaiserschmarn', 'Ueberraschung', '8.90', '14');

-- --------------------------------------------------------Fisch 15(FI)------------------------------------------------------------------------------
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('FI01', '6 Riesen Rotgarnelen', 'in Knoblauchöl, Tomaten und Zwiebelwürfel dazu Baguette', '12.50', '15');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('FI02', 'Atlantischer Lachsfilet', '', '14.50', '15');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('FI03', 'Kabeljaufilet in Senfsoße mit R&S', '', '16.50', '15');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('FI04', 'Zanderfilet in Selnfsoße mit K&S', '', '18.50', '15');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('FI05', 'Fischteller', '', '22.50', '15');

-- --------------------------------------------------------Burger 16(BU)------------------------------------------------------------------------------
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('BU01', 'Vegetarischer Burger', '', '7.50', '16');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('BU02', 'Chicken Burger', '', '8.90', '16');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('BU03', 'Gigant Burger', '', '9.50', '16');

-- --------------------------------------------------------Klassiker 17(KL)------------------------------------------------------------------------------
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('KL01', 'Currywurst Pommes', '', '7.00', '17');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('KL02', 'Bratkartoffeln mit Spiegelei & Salatgarnitur', '', '10.00', '17');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('KL03', 'Bauernfrühstück', '', '11.00', '17');

-- ----------------------------------------------------------Salate 20(SA)---------------------------------------------------------------------------
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('SA02', 'Kleiner gemischter Salat', '', '4.50', '20');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('SA02', 'Gemischter Salat Schinken & Käse', '', '8.90', '20');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('SA02', 'Gemischter Salat Thunfisch, Ei, Oliven', '', '9.50', '20');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('SA02', 'Griechischer Salat', '', '10.50',  '20');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('SA02', 'Amerikanischer Salat', '', '11.50', '20');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('SA02', 'Salat mit Rinderstreifen', '', '11.90', '20');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('SA02', 'Salat mit Riesengarnelen', '', '12.50', '20');

-- ----------------------------------------------------------Schnitzel 22(SZ) -------------------------------------------------------------------------
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('SZ01', 'Schweineschnitzel Wiener Art', '', '12.50', '22');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('SZ01', 'Schweineschnitzel Parieser Art', '', '12.50', '22');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('SZ01', 'Champignonschnitzel mit Reis & Salat', '', '13.90', '22');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('SZ01', 'Hähnchenschnitzel Wiener Art', '', '13.50', '22');

-- ---------------------------------------------------------Steak 23(SK)--------------------------------------------------------------------------------
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('SK01', 'Hähnchensteak mit Ofenkartoffeln', 'Hähnchensteak mit Ofenkartoffeln gefüllt mit Sourcreame und dazu einen Salat', '14.50', '23');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('SK02', 'Pfeffersteak Pommes Champ.', 'Pfeffersteak mit Pommes, Knoblauchbaguette und Slat', '20.50', '23');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('SK03', 'Schweinefiletmedaillons Pommes Champ.', 'Schweinemedallons mit Pommes, Champingnonrahmsoße und einen Salat', '15.50', '23');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('SK04', 'Hüftsteak Natur m. OfenK. B&S', 'Hüftsteak Natur mit Ofenkartoffeln, Sour Creame, Knoblauchbaguette und Salat', '21.50', '23');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('SK05', 'Zwiebelrostbraten m. Krok. B&S', 'Zwiebelrostbraten mit Krokretten, Knoblauchbaguette und Salat', '21.50', '23');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('SK06', 'Grillteller mit Rind, Schwein, Hähnchen P&S', 'Grillteller mit Rind, Schwein, Hähnchen Pommes und Salat', '18.50', '23');

-- ---------------------------------------------------------------Desserts 24(D)------------------------------------------------------------------------
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('D01', 'Hausgemachter Grießbrei mit Kirschen', '', '4.50', '24');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('D02', 'Hamburger rote Grütze mit Vanilleeis', '', '4.50', '24');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('D03', 'Schokoküchlein mit Flüssigen Kern', '', '7.50', '24');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('D04', 'Tiramisu mit einer Tasse Kaffee', '', '7.50', '24');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('D05', 'Pfannenkuchen mit Z&Z', '', '5.50', '24');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('D06', 'Pfannenkuchen mit Eis', '', '5.90', '24');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('D07', 'Pfannenkuchen mit Apfel', '', '6.50', '24');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('D08', 'Pfannenkuchen mit Bananen', '', '6.90', '24');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('D09', 'Pfannenkuchen mit Früüchten', '', '7.50', '24');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('D10', 'Kugel Eis', '', '1.20', '24');

-- ---------------------------------------------------------------Weihnachten 25(WH)----------------------------------------------------------------
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('F02', 'Rabatt 10%', 'Rabatt', '10', '25');

-- ---------------------------------------------------------------Kinderkarte 26(KK)----------------------------------------------------------------
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('KK01', 'Nudeln mit Butter', '', '4.50', '26');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('KK02', 'Chicken Nuggets & Pommes', '', '6.00', '26');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('KK03', 'Fischstäbchen & Pommes', '', '5.00', '26');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('KK04', 'Kleines Schnitzel Pommes', '', '6.50', '26');

-- ---------------------------------------------------------------Menks Knolle 27(MK)----------------------------------------------------------------
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('MK01', 'Knolle & Salatgarnutur', '', '6', '27');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('MK02', 'Knolle Hähnchenstreifen & Salatgarnutur', '', '10.90', '27');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('MK03', 'Knolle Riesengarnelen & Salatgarnutur', '', '12.50', '27');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('MK04', 'Knolle Lachs & Salatgarnutur', '', '14.50', '27');

-- ---------------------------------------------------------------Beilagen 28(Bei)----------------------------------------------------------------
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('Bei01', 'Pommes', '', '2.00', '28');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('Bei01', 'Ketchup/ Majo', '', '0.50', '28');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('Bei01', 'Kroketten', '', '2.00', '28');
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('Bei01', 'Reis', '', '2.00', '28');

-- ---------------------------------------------------------------Rabatte 99(R)---------------------------------------------------------------
INSERT INTO `michael`.`products` (`product_code`, `product_name`, `description`, `list_price`, `category`) VALUES ('F02', 'Rabatt 10%', 'Rabatt', '10', '99');

INSERT INTO `michael`.`orders_status` (`status_name`) VALUES ('new');
INSERT INTO `michael`.`orders_status` (`status_name`) VALUES ('done');
INSERT INTO `michael`.`orders_status` (`status_name`) VALUES ('split');
INSERT INTO `michael`.`orders_status` (`status_name`) VALUES ('payed');

INSERT INTO `michael`.`employees` (`last_name`, `first_name`, `job_title`, `address`, `city`, `state_province`, `zip_postal_code`, `country_region`, `notes`) VALUES ('Maierhofer', 'Sandro', 'Admin', 'Munketoft 32', 'Flensburg', 'Schleswig-Holstein', '24937', '', 'Creator');
INSERT INTO `michael`.`employees` (`last_name`, `first_name`, `job_title`, `address`, `city`, `state_province`, `zip_postal_code`, `notes`) VALUES ('Maierhofer', 'Shahin', 'Chef', 'An de Diekwisch 7', 'Hanstedt', 'Niedersachsen', '21271', 'Chef and Cook');
INSERT INTO `michael`.`employees` (`last_name`, `first_name`, `job_title`, `address`, `city`, `state_province`, `zip_postal_code`, `notes`) VALUES ('Maierhofer', 'Waltraud', 'Chef', 'An de Diekwisch 7', 'Hanstedt', 'Niedersachsen', '21271', 'Chef and Service');

INSERT INTO `michael`.`bill_status` (`status_name`) VALUES ('open');
INSERT INTO `michael`.`bill_status` (`status_name`) VALUES ('done');
INSERT INTO `michael`.`bill_status` (`status_name`) VALUES ('storno');

-- -------Role : "admin", "manager", "service", "kitchen"
INSERT INTO `michael`.`user` (`username`, `password`, `role`, `employee_id`, `status`) VALUES ('sandro_maierhofer', '8Q3Qs6ZOzZZnek/CthvxOIVfGyvUCrA0pP0R8hveVPg=', 'admin', '1', '1');
INSERT INTO `michael`.`user` (`username`, `password`, `role`, `employee_id`, `status`) VALUES ('waltraud_maierhofer','8Q3Qs6ZOzZZnek/CthvxOIVfGyvUCrA0pP0R8hveVPg=', 'manager', '3', '1');
INSERT INTO `michael`.`user` (`username`, `password`, `role`, `employee_id`, `status`) VALUES ('shahin_maierhofer', '8Q3Qs6ZOzZZnek/CthvxOIVfGyvUCrA0pP0R8hveVPg=', 'service', '2', '1');

INSERT INTO `michael`.`transaction_types` (`type_name`) VALUES ('Gutschein');
INSERT INTO `michael`.`transaction_types` (`type_name`) VALUES ('Kreditkarte');
INSERT INTO `michael`.`transaction_types` (`type_name`) VALUES ('Bar');

