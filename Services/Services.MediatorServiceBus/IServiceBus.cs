﻿using System;

namespace De.SandroMaierhofer.POS_Michael.Services.MediatorServiceBus
{
    interface IServiceBus
    {
        void Register<TNotification>(object listener, Action<TNotification> action);
        void Send<TNotification>(TNotification notification);
        void Unregister<TNotification>(object listener);
    }
}
