﻿using System;

namespace De.SandroMaierhofer.POS_Michael.Services.MediatorServiceBus
{
    class WeakReferenceParameterAction<ParameterType> : WeakReferenceAction
    {
        private Action<ParameterType> action;
        public WeakReferenceParameterAction(object target, Action<ParameterType> action)
            : base(target, null)
        {
            this.action = action;
        }
        public override void Execute()
        {
            if (action != null && Target != null && Target.IsAlive)
                action(default(ParameterType));
        }
        public void Execute(ParameterType parameter)
        {
            if (action != null && Target != null && Target.IsAlive)
                this.action(parameter);
        }
        public Action<ParameterType> Action
        {
            get
            {
                return action;
            }
        }
    }
}