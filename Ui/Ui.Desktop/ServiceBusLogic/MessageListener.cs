﻿using De.SandroMaierhofer.POS_Michael.Services.MediatorServiceBus;
using De.SandroMaierhofer.POS_Michael.Logic.Ui.Messages;

namespace De.SandroMaierhofer.POS_Michael.Ui.Desktop.ServiceBusLogic
{
    public class MessageListener
    {
        public MessageListener()
        {
            InitMessenger();
        }

        #region methods

        private void InitMessenger()
        {
            ServiceBus.Instance.Register<OpenTablesWindow>(
                this,
                msg =>
                {
                    TableWindow = new TablesWindow();
                    TableWindow.ShowDialog();
                });
            ServiceBus.Instance.Register<CloseTablesWindow>(
                this,
                msg =>
                {
                    TableWindow.Close();
                });
            ServiceBus.Instance.Register<ClosePayedWindow>(
                this,
                msg =>
                {
                    PayedWIndow.Close();
                });
            ServiceBus.Instance.Register<OpenBillWindow>(
                this,
                msg =>
                {
                    BillWindow = new BillWindow();
                    BillWindow.ShowDialog();
                });
            ServiceBus.Instance.Register<CloseBillWindow>(
                this,
                msg =>
                {

                    BillWindow.Close();
                });
            ServiceBus.Instance.Register<OpenPayedWIndow>(
               this,
               msg =>
               {
                   PayedWIndow = new PayedWIndow();
                   PayedWIndow.ShowDialog();
               });
        }


        #endregion

        #region properties

        //  Damit diese Property gebindet werden kann, muss zunächst 
        //  diese Klasse als Static Resource der App.xaml hinzugefügt werden:

        //<Application.Resources>
        //<desktop:MessageListener x:Key="MessageListener">
        //</desktop:MessageListener>
        //</Application.Resources>
        //</Application>

        // Dann muss nur noch die Property an die IsEnabled-Eigenschaft gebindet werden,
        // damit eine Instanz von dieser Klasse erzeugt wird:

        //  <Window.IsEnabled>
        //          <Binding Source = "{StaticResource MessageListener}" Path="BindableProperty"></Binding>
        //  </Window.IsEnabled>

        // Und nur dafür ist diese Property... :
        public bool BindableProperty => true;

        private TablesWindow TableWindow;
        private BillWindow BillWindow;
        private PayedWIndow PayedWIndow;
        #endregion
    }
}
